cmake_minimum_required(VERSION 2.8.3)
project(gnomic_ros)

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --std=gnu++11") FLAG PRECEDENTI
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=c++11 -Ofast -march=native") 

set(CMAKE_BUILD_TYPE Release)

message(STATUS "PROCESSOR_TYPE: [${CMAKE_HOST_SYSTEM_PROCESSOR}]")
if (${CMAKE_HOST_SYSTEM_PROCESSOR} MATCHES armv7l)
  set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -march=armv7-a -mfpu=neon-vfpv4 -mfloat-abi=hard -funsafe-math-optimizations")
  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=armv7-a -mfpu=neon-vfpv4 -mfloat-abi=hard -funsafe-math-optimizations")
  message("ENABLING ARM NEON OPTIMIZATIONS")
endif ()


find_package(Eigen3 REQUIRED)

# Find Catkin to manage compilation
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rosbag
  nav_msgs
  sensor_msgs
  mav_msgs
  std_srvs
  gazebo_msgs
  gnomic
)

if(gnomic_FOUND)
  message("GNOMIC Found: " ${gnomic_INCLUDE_DIRS})
else()
  message("GNOMIC NOT Found!")
endif()


catkin_package(
  INCLUDE_DIRS src 
  LIBRARIES gnomic_mav_library gnomic_youbot_library gnomic_turtlebot_library
  CATKIN_DEPENDS gnomic
)


include_directories(
  ${catkin_INCLUDE_DIRS}
  ${gnomic_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIRS}
  ${PROJECT_SOURCE_DIRS}/src
  src
)

add_subdirectory(${PROJECT_SOURCE_DIR}/src)
