#include <gnomic_ros/gnomic_youbot.h>

using namespace gnomic;
using namespace youbot_model;

int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "NMPC_node");
  ros::NodeHandle nh("~");
  GnomicNodeYoubot node(nh);
  
  std::string filename;
  if(argv[1])
    filename = argv[1];
  else
    filename = "/home/bellisario/Scrivania/quaterback_cose/quaterbackls/params/params_youbot.yaml";
  
  std::cout << " Configuration filename: " << filename << std::endl;
  node.initFromYaml(filename);

  node.activatePublisher("/cmd_vel");
  node.activateCallback("/odom");
  node.activateTrajectoryCallback("/command/pose");

  node.setVerbosity();
  
  /// Init & Set Final StateVector
  youbot_model::StateVector final_state;
  final_state.p = gnomic::Vector2( 0, 0);
  final_state.yaw = 2;
  final_state.d_p = gnomic::Vector2(0,0);
  node.setFinalState(final_state);
  
  ros::spin();
  
  return 0;
  
}
