#include <gnomic_ros/gnomic_turtlebot.h>

using namespace gnomic;
using namespace turtlebot_model;

int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "NMPC_node");
  ros::NodeHandle nh("~");
  GnomicNodeTurtlebot node(nh);
  
  std::string filename;
  if(argv[1])
    filename = argv[1];
  else
    filename = "/home/bellisario/Scrivania/quaterback_cose/quaterbackls/params/params_youbot.yaml";
  
  std::cout << " Configuration filename: " << filename << std::endl;
  node.initFromYaml(filename);

  node.activatePublisher("/cmd_vel_mux/input/navi");
  node.activateCallback("/odom");
  node.activateTrajectoryCallback("/command/pose");

  node.setVerbosity();
  
  /// Init & Set Final StateVector
  StateVector final_state;
  final_state.p = gnomic::Vector2(1, 0);
  final_state.yaw = -1;
  final_state.v = 0;
  node.setFinalState(final_state);
  
  ros::spin();
  
  return 0;
  
}
