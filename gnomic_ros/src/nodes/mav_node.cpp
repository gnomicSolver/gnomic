#include <gnomic_ros/gnomic_mav.h>

using namespace gnomic;
using namespace mav_model;

int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "NMPC_node");
  ros::NodeHandle nh("~");
  GnomicNodeMav node(nh);
  
  std::string filename;
  if(argv[1])
    filename = argv[1];
  else
    filename = "/home/bellisario/Scrivania/quaterback_cose/quaterbackls/params/params_mav.yaml";
  
  std::cout << " Configuration filename: " << filename << std::endl;
  node.initFromYaml(filename);

  node.activatePublisher("/command/roll_pitch_yawrate_thrust");
  node.activateCallback("/firefly/ground_truth/odometry");
  node.activateTrajectoryCallback("/command/pose");
  
  node.setVerbosity();
  
  /// Init & Set Final StateVector
  StateVector final_state;
  final_state.p = gnomic::Vector3( 1, 0, 1);
  final_state.q = gnomic::AngleAxis( 0, gnomic::Vector3::UnitZ() );
  node.setFinalState(final_state);
  
  ros::spin();
  
  return 0;
  
}
