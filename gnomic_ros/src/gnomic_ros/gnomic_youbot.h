#pragma once

#include <iostream>
#include <gnomic/solver.h>
#include <gnomic/loss.h>

#include <youbot_model/youbot_dynamics.h>
#include <youbot_model/input_vector.h>
#include <youbot_model/state_vector.h>
#include <youbot_model/xi.h>

#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <mav_msgs/default_topics.h>
#include <mav_msgs/RollPitchYawrateThrust.h>

#include <yaml-cpp/yaml.h>


class GnomicNodeYoubot{
 public:
  typedef gnomic::Loss<youbot_model::InputVector, youbot_model::StateVector, youbot_model::Xi> YoubotLoss;
  
  GnomicNodeYoubot(ros::NodeHandle nh);
    
  ~GnomicNodeYoubot(){
    std::cerr << "[GnomicNodeYoubot] deleting\n";
    delete _solver;
    delete _robot;
    std::cerr << "[GnomicNodeYoubot] deleted\n";
  }

  void initFromYaml(const std::string& yaml_file);

  void odometryCallback(const nav_msgs::OdometryConstPtr& odom_msg);
  void trajectoryCallback(const nav_msgs::OdometryConstPtr& traj_msg);
  void activateCallback(const std::string&);
  void activatePublisher(const std::string&);
  void activateTrajectoryCallback(const std::string&);

  void setFinalState(const youbot_model::StateVector& final_state){
    _final_state = final_state;
    if(_solver)
      _solver->setFinalState(&_final_state);
    else
      throw std::runtime_error("init the node first!\n");
  }
  void setVerbosity() {
    if(_solver)
      _solver->setVerbosity(true);
    else
      throw std::runtime_error("init the node first!\n");
  }
    
 protected:

  YoubotLoss* _loss;
  gnomic::Solver<YoubotLoss>* _solver;
  gnomic::BaseRobotDynamics<youbot_model::InputVector, youbot_model::StateVector, youbot_model::Xi>* _robot;

  youbot_model::StateVector _final_state;

  gnomic::real _dyn_tf;   // trajectory time dynamically adjusted
  int _dyn_N;    // horizon dynamically adjusted
  gnomic::real _dyn_N_step_time;
  ros::Time _update_target_time;
  gnomic::real _tf;       // trajectory time
  int _N;          // horizon
  int _iterations; // max iterations
  bool _target_updated;

  ros::Time _prev_time;
  bool _isFirstOdomMsg;
  bool _enable_integrator;
  bool _update_goal;
  gnomic::real _antiwindup_ball;
  gnomic::Vector2 _position_error_integration;
  gnomic::Vector2 _integral_cmd;
  gnomic::real _position_error_integration_limit;
  gnomic::real _angle_error_integration;

  gnomic::Vector2 _start_posit;
  gnomic::real _start_yaw;
    
  gnomic::Vector3 _current_odom_position;
  gnomic::real _current_yaw_orientation;
    
  gnomic::Matrix3 _I; //inertia_matrix
  gnomic::real _mass;
    
  std::vector<gnomic::real> _pose_factors;
  std::vector<gnomic::real> _linear_velocity_factors;
  gnomic::real _yaw_rate_factor, _orientation_factors;
  gnomic::real _input_vx_factor, _input_vy_factor, _input_omega_factor;

  // Time Mesh Refinement
  bool _timeMeshRefinement;
  int _max_pyramidal_refs;
  gnomic::real _epsilon;

  bool _verbosity;
  
  //node handle
  ros::NodeHandle _nh;

  //subscriber
  ros::Subscriber _odom_sub, _traj_sub;

  //publisher
  ros::Publisher _cmd_pub;

  //param filename
  std::string _yaml_filename;
    
};
