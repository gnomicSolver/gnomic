#include "gnomic_mav.h"

GnomicNodeMav::GnomicNodeMav(ros::NodeHandle nh) {
  _loss = 0;
  _robot = 0;
  _solver = 0;
  
  _nh = nh;
  _tf = 0.f;
  _dyn_tf = 0.f;
  _N = 0;
  _dyn_N = 0;
  _dyn_N_step_time = 0.f;
  _iterations = 0;
  _yaw_rate_factor = 0.f;
  _mass = 0.f;
  _I = gnomic::Matrix3::Identity();
  _DragMatrix = gnomic::Matrix3::Identity();
  _prev_time = ros::Time::now();
  _isFirstOdomMsg = true;
  _enable_integrator = true;
  _target_updated = false;
  _update_goal = true;
  _antiwindup_ball = 0.2f;
  _position_error_integration.setZero();
  _position_error_integration_limit = .3f;
  _integral_cmd.setZero();
  _input_tau1_factor = 0.f;
  _input_tau2_factor = 0.f;
  _input_tau3_factor = 0.f;
  _input_thrust_factor = 0.f;
  
  _final_state.p = gnomic::Vector3::Zero();
  _final_state.q = gnomic::AngleAxis( 1, gnomic::Vector3::UnitZ() );
  
  _current_odom_position.setZero();
  _current_yaw_orientation = 0.f;

  _verbosity = false;
  
  _imu_sub = _nh.subscribe("/firefly/imu", 1, &GnomicNodeMav::imuCallback, this, ros::TransportHints().tcpNoDelay());
  srv_play = nh.serviceClient<std_srvs::Empty>("/gazebo/unpause_physics");
  srv_pause = nh.serviceClient<std_srvs::Empty>("/gazebo/pause_physics");
  
}

void GnomicNodeMav::initFromYaml(const std::string &yaml_file) {

  // store the yaml filename for possible re-loading
  _yaml_filename = yaml_file;
  
  YAML::Node configuration = YAML::LoadFile(yaml_file);
  _inertia_params = configuration["dynamic_parameters"]["inertia"].as<std::vector<gnomic::real>>();
  _mass = configuration["dynamic_parameters"]["mass"].as<gnomic::real>();
  _drag_coefficients = configuration["dynamic_parameters"]["drag_coefficients"].as<std::vector<gnomic::real>>();

  /// Init Robot
  _DragMatrix.diagonal() << _drag_coefficients[0], _drag_coefficients[1], _drag_coefficients[2];
  _I.diagonal() << _inertia_params[0], _inertia_params[1], _inertia_params[2];
  _robot = new mav_model::MavDynamics<mav_model::InputVector, mav_model::StateVector, mav_model::Xi>(_mass, _I);
  //_robot->setDragMatrix(_DragMatrix);

  /// Init Loss
  _loss = new MavLoss();
  _loss->setRobotDynamics(_robot);
   
  /// Init Solver
  _solver = new gnomic::Solver<MavLoss>();

  _N = configuration["temporal_parameters"]["N"].as<int>();
  _dyn_N = _N;
  _iterations = configuration["temporal_parameters"]["iterations"].as<int>();
  _tf = configuration["temporal_parameters"]["Tf"].as<gnomic::real>();
  _dyn_tf = _tf;

  _solver->setIterations(_iterations);
  _solver->setTimeVariables(_dyn_tf, _dyn_N);

  /// Setting Weights
  MavLoss::Weights weights;
  
  // Final State Weight Matrix   
  _pose_factors = configuration["final_state_factors"]["pose_factors"].as<std::vector<gnomic::real>>();
  _orientation_factors = configuration["final_state_factors"]["orientation_factors"].as<std::vector<gnomic::real>>();
  _linear_velocity_factors = configuration["final_state_factors"]["linear_velocity_factors"].as<std::vector<gnomic::real>>();
  _yaw_rate_factor = configuration["final_state_factors"]["yaw_rate_factor"].as<gnomic::real>();
  weights.Sf.diagonal() << _pose_factors[0], _pose_factors[1], _pose_factors[2],
    _orientation_factors[0], _orientation_factors[1], _orientation_factors[2],
    _linear_velocity_factors[0], _linear_velocity_factors[1],
    _linear_velocity_factors[2], _yaw_rate_factor;  
     
  // Dynamics Weight Matrix       
  _pose_factors = configuration["dynamic_state_factors"]["pose_factors"].as<std::vector<gnomic::real>>();
  _orientation_factors = configuration["dynamic_state_factors"]["orientation_factors"].as<std::vector<gnomic::real>>();
  _linear_velocity_factors = configuration["dynamic_state_factors"]["linear_velocity_factors"].as<std::vector<gnomic::real>>();
  _yaw_rate_factor = configuration["dynamic_state_factors"]["yaw_rate_factor"].as<gnomic::real>();         
  weights.Al.diagonal() << _pose_factors[0], _pose_factors[1], _pose_factors[2],
    _orientation_factors[0], _orientation_factors[1], _orientation_factors[2],
    _linear_velocity_factors[0], _linear_velocity_factors[1],
    _linear_velocity_factors[2], _yaw_rate_factor;     
  
  _input_tau1_factor = configuration["control_input_factors"]["roll_factor"].as<gnomic::real>();
  _input_tau2_factor = configuration["control_input_factors"]["pitch_factor"].as<gnomic::real>();
  _input_tau3_factor = configuration["control_input_factors"]["yaw_rate_factor"].as<gnomic::real>();
  _input_thrust_factor = configuration["control_input_factors"]["thrust_factor"].as<gnomic::real>();
  weights.R.diagonal() << _input_tau1_factor, _input_tau2_factor, _input_tau3_factor, _input_thrust_factor;

  _timeMeshRefinement = configuration["temporal_parameters"]["perform_time_mesh refinement"].as<int>();
  _epsilon = configuration["temporal_parameters"]["discretization_error"].as<gnomic::real>();
  _max_pyramidal_refs = configuration["temporal_parameters"]["max_pyramidal_refinement"].as<int>();
  
  _solver->setTimeMeshRefinement(_timeMeshRefinement, _max_pyramidal_refs, _epsilon);

  _verbosity = configuration["solver_verbosity"]["verbosity"].as<int>();
  _solver->setVerbosity(_verbosity);
  
  _loss->setWeights(weights);    
  _solver->setLoss(_loss);

  
  _solver->setFinalState(&_final_state);
  _target_updated = true;

  std::cerr << "InitFromYAML done!" << std::endl;
  
}

void GnomicNodeMav::imuCallback(const sensor_msgs::ImuConstPtr& imu_msg)
{
  _last_imu = *imu_msg;
}


void GnomicNodeMav::activateCallback(const std::string &odom_callback_name) {
  _odom_sub =
    _nh.subscribe(odom_callback_name, 1, &GnomicNodeMav::odometryCallback,
		  this, ros::TransportHints().tcpNoDelay());
}

void GnomicNodeMav::activatePublisher(const std::string &cmd_publisher_name) {
  _cmd_pub =
    _nh.advertise<mav_msgs::RollPitchYawrateThrust>(cmd_publisher_name, 1);
}

void GnomicNodeMav::activateTrajectoryCallback(const std::string &traj_callback_name) {
  _traj_sub = _nh.subscribe(traj_callback_name, 1, &GnomicNodeMav::trajectoryCallback,
			    this, ros::TransportHints().tcpNoDelay());
}

void GnomicNodeMav::trajectoryCallback(const nav_msgs::OdometryConstPtr& traj_msg) {
  
  gnomic::Vector3 position( traj_msg->pose.pose.position.x,
			    traj_msg->pose.pose.position.y,
			    traj_msg->pose.pose.position.z);
  
  gnomic::Quaternion attitude( traj_msg->pose.pose.orientation.w,
			       traj_msg->pose.pose.orientation.x,
			       traj_msg->pose.pose.orientation.y,
			       traj_msg->pose.pose.orientation.z);
    
  gnomic::Vector3 velocity( traj_msg->twist.twist.linear.x,
			    traj_msg->twist.twist.linear.y,
			    traj_msg->twist.twist.linear.z);
  
  attitude.normalize();
    
  /// Init & Set Final StateVector
  mav_model::StateVector final_state;
  final_state.p = position;  
    
  final_state.q = attitude;
  //final_state.q = Eigen::Quaternionf(0.707,0,0,0.707);
  
  final_state.dot_p = velocity; 
  
  final_state.dot_psi = traj_msg->twist.twist.angular.z;
  
  _target_updated = true;
  _update_goal = true;
  
  setFinalState(final_state);
}


void GnomicNodeMav::odometryCallback(
				     const nav_msgs::OdometryConstPtr &odom_msg) {
  if (_isFirstOdomMsg) {
    _prev_time = ros::Time::now();
    _isFirstOdomMsg = false;
  }

  
  gnomic::Quaternion q(
		       odom_msg->pose.pose.orientation.w, odom_msg->pose.pose.orientation.x,
		       odom_msg->pose.pose.orientation.y, odom_msg->pose.pose.orientation.z);

  gnomic::Vector3 position_W(odom_msg->pose.pose.position.x,
                             odom_msg->pose.pose.position.y,
                             odom_msg->pose.pose.position.z);

  _current_odom_position = position_W;

  q.normalize();
  gnomic::Matrix3 q_mat = q.toRotationMatrix();
  _current_yaw_orientation = gnomic::yawFromQuaternion(q);
    
  gnomic::Vector3 velocity_W(q_mat *
                             gnomic::Vector3(odom_msg->twist.twist.linear.x,
                                             odom_msg->twist.twist.linear.y,
                                             odom_msg->twist.twist.linear.z));

  gnomic::Vector3 omega_W(q_mat *
                          gnomic::Vector3(odom_msg->twist.twist.angular.x,
                                          odom_msg->twist.twist.angular.y,
                                          odom_msg->twist.twist.angular.z));

  
  // check if publisher has been activated
  if(_cmd_pub) {
    if(_target_updated) {
      _dyn_tf = _tf;
      _dyn_N = _N;
      _dyn_N_step_time = _tf/_N;
      _update_target_time = ros::Time::now();
      _target_updated = false;
      _start_posit = position_W;
      _start_yaw = _current_yaw_orientation;
    }
    
    gnomic::real _start_dist = (_start_posit - _solver->getFinalState()->p).norm() + fabs(_start_yaw - gnomic::yawFromQuaternion(_solver->getFinalState()->q)); 
    gnomic::real _curr_dist = (position_W - _solver->getFinalState()->p).norm() + fabs(_current_yaw_orientation - gnomic::yawFromQuaternion(_solver->getFinalState()->q)); 
    _dyn_tf = ( _tf*_curr_dist )/_start_dist;
    
    if(_dyn_tf < 1)
      _dyn_tf = 1;
    
    gnomic::real dt_nominale = _tf/_N;
    
    _dyn_N = ceil(_dyn_tf/dt_nominale);
    _dyn_N = fmin(_dyn_N, _N);
    _dyn_tf = fmin(_dyn_tf, _tf);
    
    if(_verbosity)
        std::cerr << FGRN("[SOLVER][dynamic time mesh]") << " Tf: " << _dyn_tf << " N: " << _dyn_N << std::endl; 
    
    _solver->setTimeVariables(_dyn_tf, _dyn_N);
  }
  
  gnomic::Vector3 accel_W;
  if((velocity_W - _prev_vel).norm() > 0.01){
    accel_W = (velocity_W - _prev_vel)/( odom_msg->header.stamp.toSec() - _prev_time.toSec() );
  }
  else{
    accel_W = gnomic::Vector3(0.f,0.f,0.f);
  }
//   std::cout << velocity_W.transpose() << " " << _prev_vel.transpose() << std::endl;
//   std::cout << (velocity_W - _prev_vel).transpose() << " " << ( odom_msg->header.stamp.toSec() - _prev_time.toSec() ) << std::endl;
  _prev_vel = velocity_W;
  
  // Initial StateVector
  mav_model::StateVector init_state;
  init_state.p = position_W;
  init_state.q = q;
  init_state.dot_p = velocity_W;
  init_state.omega = omega_W;
  init_state.ddot_p = accel_W;
  //init_state.thrust = _mass * ( accel_W).norm();
  
  // Initial InputVector
  mav_model::InputVector initial_input;
  initial_input.thrust = init_state.thrust;

  _solver->setInitialState(&init_state);
  _solver->setInitialInput(&initial_input);
  
  if(_update_goal) {
    _update_goal = false;
    _solver->init(gnomic::TrajectoryInitialization::LINEAR); 
  }
  else
    _solver->init(gnomic::TrajectoryInitialization::PREVIOUS);

//   std::cout <<  position_W.transpose() << std::endl;
//   std::cout <<  q.vec().transpose() << std::endl;
//   std::cout <<  velocity_W.transpose() << std::endl;
//   std::cout <<  omega_W.transpose() << std::endl;
//   std::cout << -gnomic::gravity * _mass << std::endl;
  
  std::cout << "p:         " << init_state.p.transpose() << std::endl
            << "dp:        " << init_state.dot_p.transpose() << std::endl
            << "omega:     " << init_state.omega.transpose() << std::endl
            << "ddot_p:    " << init_state.ddot_p.transpose() << std::endl;
  
  std_srvs::Empty empty;
  srv_pause.call(empty);
  _solver->compute();
  srv_play.call(empty);
  
  std::vector<mav_model::Xi> outputTrajectory = _solver->optimalTrajectory();

  printOptimalTrajectory(outputTrajectory);
  
  
  mav_model::StateVector init_state2;
  init_state2.p = position_W;
  init_state2.q = q;
  init_state2.dot_p = velocity_W;
  init_state2.omega = omega_W;
  init_state2.ddot_p = accel_W;
  init_state2.thrust = _mass * ( accel_W).norm();
  
//   std::cout <<  init_state2.p.transpose() << std::endl;
//   std::cout <<  init_state2.q.vec().transpose() << std::endl;
//   std::cout <<  init_state2.dot_p.transpose() << std::endl;
//   std::cout <<  init_state2.omega.transpose() << std::endl;
//   std::cout <<  init_state2.thrust << std::endl;
  
  mav_model::StateVector state;
  //_robot->computeState(state, init_state2, outputTrajectory[0], outputTrajectory[1], _solver->dt(0));
  
  
  
  
  _robot->computeState(state, init_state2, outputTrajectory[0], outputTrajectory[1], _solver->dt(0));
  
  
  
  std::cout << "dt:        " << _solver->dt(0) << std::endl
	    << "p:         " << state.p.transpose() << std::endl
            << "dp:        " << state.dot_p.transpose() << std::endl
            << "omega:     " << state.omega.transpose() << std::endl
            << "ddot_p:    " << state.ddot_p.transpose() << std::endl;
   
  mav_model::InputVector input;
  _robot->computeInput(state, input);

  mav_msgs::RollPitchYawrateThrust rpyr_cmd;
  gnomic::Vector3 yaw_rate_W(state.q.toRotationMatrix().inverse() *
                             state.omega);
  
  gnomic::real sampling_time = (ros::Time::now() - _prev_time).toSec();
  _prev_time = ros::Time::now();
  
  if (_enable_integrator) {

    gnomic::Vector3 position_error = _solver->getFinalState()->p - position_W;
    gnomic::real angle_error = gnomic::yawFromQuaternion( _solver->getFinalState()->q ) - _current_yaw_orientation;

    if (position_error.norm() < _antiwindup_ball) {
      _position_error_integration += position_error * sampling_time;
    } else {
      _position_error_integration.setZero();
    }
	
    if (fabs(angle_error) < 0.2){
      _angle_damping_factor = - 5*omega_W(2);
    } else {
      _angle_damping_factor = 0.f;
    }
	
    if (fabs(angle_error) < 0.1){
      _angle_error_integration += angle_error * sampling_time;
    } else {
      _angle_error_integration = 0.f;
    }
    
    _position_error_integration = _position_error_integration.cwiseMax(
								       gnomic::Vector3(-_position_error_integration_limit,
										       -_position_error_integration_limit,
										       -_position_error_integration_limit));
    
    _position_error_integration = _position_error_integration.cwiseMin(
								       gnomic::Vector3(_position_error_integration_limit, _position_error_integration_limit,
										       _position_error_integration_limit));

    _angle_error_integration = fmin(fmax(_angle_error_integration, -0.15), 0.15);
    
    _integral_cmd = gnomic::Vector3(.005, .005, .2).asDiagonal() *
      _position_error_integration;
  }
  
  
  _final_state.q.normalize();
  gnomic::Vector3 feed_forward_term( init_state.q.toRotationMatrix().inverse() * _final_state.dot_p ); // lo yaw in cui è calcolato la velocità della traiettoria è diverso da quello inseguito, bisogna apparare!
    
  gnomic::Vector3 omega( state.q.toRotationMatrix() * state.omega );
  
  state.q.normalize();
  rpyr_cmd.header = odom_msg->header;
  rpyr_cmd.thrust.z = state.thrust + _integral_cmd(2);// + .1*feed_forward_term(2);
  rpyr_cmd.roll = gnomic::rollFromQuaternion(state.q) + _integral_cmd(0);// - .05*feed_forward_term(1);
  rpyr_cmd.pitch = gnomic::pitchFromQuaternion(state.q) + _integral_cmd(1);// + .06*feed_forward_term(0);
  //rpyr_cmd.yaw_rate = 4*state.dot_psi + .5*_angle_damping_factor + _angle_error_integration + 1.5*_final_state.dot_psi;
  rpyr_cmd.yaw_rate = 4*state.dot_psi + 2*_angle_damping_factor + _angle_error_integration;// + 1.5*_final_state.dot_psi;
  
  std::cout << rpyr_cmd << std::endl;
    
  _cmd_pub.publish(rpyr_cmd);
}
