#include "gnomic_mav_dynamic.h"

GnomicNodeMav::GnomicNodeMav(ros::NodeHandle nh) {
  _loss = 0;
  _robot = 0;
  _solver = 0;
  
  _nh = nh;
  _tf = 0.f;
  _dyn_tf = 0.f;
  _N = 0;
  _dyn_N = 0;
  _dyn_N_step_time = 0.f;
  _iterations = 0;
  _yaw_rate_factor = 0.f;
  _mass = 0.f;
  _I = gnomic::Matrix3::Identity();
  _prev_time = ros::Time::now();
  _isFirstOdomMsg = true;
  _enable_integrator = false;
  _target_updated = false;
  _update_goal = true;
  _antiwindup_ball = 0.3f;
  _position_error_integration.setZero();
  _position_error_integration_limit = .2f;
  _integral_cmd.setZero();
  _input_tau1_factor, _input_tau2_factor, _input_tau3_factor, _input_thrust_factor = 0.f;
  
  _final_state.p = gnomic::Vector3::Zero();
  _final_state.q = gnomic::AngleAxis( 1, gnomic::Vector3::UnitZ() );
  
  _current_odom_position.setZero();
  _current_yaw_orientation = 0.f;
  
}

void GnomicNodeMav::initFromYaml(const std::string &yaml_file) {

  // store the yaml filename for possible re-loading
  _yaml_filename = yaml_file;
  
  YAML::Node configuration = YAML::LoadFile(yaml_file);
  std::vector<gnomic::real> inertia_params =
    configuration["dynamic_parameters"]["inertia"].as<std::vector<gnomic::real>>();
  _mass = configuration["dynamic_parameters"]["mass"].as<gnomic::real>();

  /// Init Robot
  _I.diagonal() << inertia_params[0], inertia_params[1], inertia_params[2];
  _robot = new mav_model::MavDynamics<mav_model::InputVector, mav_model::StateVector, mav_model::Xi>(_mass, _I);

  /// Init Loss
  _loss = new MavLoss();
  _loss->setRobotDynamics(_robot);
   
  /// Init Solver
  _solver = new gnomic::Solver<MavLoss>();

  _N = configuration["temporal_parameters"]["N"].as<int>();
  _dyn_N = _N;
  _iterations = configuration["temporal_parameters"]["iterations"].as<int>();
  _tf = configuration["temporal_parameters"]["Tf"].as<gnomic::real>();
  _dyn_tf = _tf;

  _solver->setIterations(_iterations);
  _solver->setTimeVariables(_dyn_tf, _dyn_N);

  /// Setting Weights
  MavLoss::Weights weights;
  
  // Final State Weight Matrix   
  _pose_factors = configuration["final_state_factors"]["pose_factors"].as<std::vector<gnomic::real>>();
  _orientation_factors = configuration["final_state_factors"]["orientation_factors"].as<std::vector<gnomic::real>>();
  _linear_velocity_factors = configuration["final_state_factors"]["linear_velocity_factors"].as<std::vector<gnomic::real>>();
  _yaw_rate_factor = configuration["final_state_factors"]["yaw_rate_factor"].as<gnomic::real>();
  weights.Sf.diagonal() << _pose_factors[0], _pose_factors[1], _pose_factors[2],
    _orientation_factors[0], _orientation_factors[1], _orientation_factors[2],
    _linear_velocity_factors[0], _linear_velocity_factors[1],
    _linear_velocity_factors[2], _yaw_rate_factor;  
     
  // Dynamics Weight Matrix       
  _pose_factors = configuration["dynamic_state_factors"]["pose_factors"].as<std::vector<gnomic::real>>();
  _orientation_factors = configuration["dynamic_state_factors"]["orientation_factors"].as<std::vector<gnomic::real>>();
  _linear_velocity_factors = configuration["dynamic_state_factors"]["linear_velocity_factors"].as<std::vector<gnomic::real>>();
  _yaw_rate_factor = configuration["dynamic_state_factors"]["yaw_rate_factor"].as<gnomic::real>();         
  weights.Al.diagonal() << _pose_factors[0], _pose_factors[1], _pose_factors[2],
    _orientation_factors[0], _orientation_factors[1], _orientation_factors[2],
    _linear_velocity_factors[0], _linear_velocity_factors[1],
    _linear_velocity_factors[2], _yaw_rate_factor;     
  
  _input_tau1_factor = configuration["control_input_factors"]["roll_factor"].as<gnomic::real>();
  _input_tau2_factor = configuration["control_input_factors"]["pitch_factor"].as<gnomic::real>();
  _input_tau3_factor = configuration["control_input_factors"]["yaw_rate_factor"].as<gnomic::real>();
  _input_thrust_factor = configuration["control_input_factors"]["thrust_factor"].as<gnomic::real>();
  weights.R.diagonal() << _input_tau1_factor, _input_tau2_factor, _input_tau3_factor, _input_thrust_factor;
		
  _loss->setWeights(weights);    
  _solver->setLoss(_loss);
  
  _solver->setFinalState(&_final_state);
  _target_updated = true;

  std::cerr << "InitFromYAML done!" << std::endl;
  
}

void GnomicNodeMav::activateCallback(const std::string &odom_callback_name) {
  _odom_sub =
    _nh.subscribe(odom_callback_name, 1, &GnomicNodeMav::odometryCallback,
		  this, ros::TransportHints().tcpNoDelay());
}

void GnomicNodeMav::activatePublisher(const std::string &cmd_publisher_name) {
  _cmd_pub =
    _nh.advertise<mav_msgs::RollPitchYawrateThrust>(cmd_publisher_name, 1);
}

void GnomicNodeMav::activateTrajectoryCallback(const std::string &traj_callback_name) {
  _traj_sub = _nh.subscribe(traj_callback_name, 1, &GnomicNodeMav::trajectoryCallback,
			    this, ros::TransportHints().tcpNoDelay());
}

void GnomicNodeMav::trajectoryCallback(const nav_msgs::OdometryConstPtr& traj_msg) {
  
  gnomic::Vector3 position( traj_msg->pose.pose.position.x,
			    traj_msg->pose.pose.position.y,
			    traj_msg->pose.pose.position.z);
  
  gnomic::Quaternion attitude( traj_msg->pose.pose.orientation.w,
			       traj_msg->pose.pose.orientation.x,
			       traj_msg->pose.pose.orientation.y,
			       traj_msg->pose.pose.orientation.z);
    
  gnomic::Vector3 velocity( traj_msg->twist.twist.linear.x,
			    traj_msg->twist.twist.linear.y,
			    traj_msg->twist.twist.linear.z);
    
  attitude.normalize();
    
  /// Init & Set Final StateVector
  mav_model::StateVector final_state;
  final_state.p = position;  
    
  final_state.q = attitude;
  final_state.dot_p = velocity; 
  
  _update_goal = true;
    
  setFinalState(final_state);
}

void GnomicNodeMav::odometryCallback(
				     const nav_msgs::OdometryConstPtr &odom_msg) {
  if (_isFirstOdomMsg) {
    _prev_time = ros::Time::now();
    _isFirstOdomMsg = false;
  }

  
  gnomic::Quaternion q(
		       odom_msg->pose.pose.orientation.w, odom_msg->pose.pose.orientation.x,
		       odom_msg->pose.pose.orientation.y, odom_msg->pose.pose.orientation.z);

  gnomic::Vector3 position_W(odom_msg->pose.pose.position.x,
                             odom_msg->pose.pose.position.y,
                             odom_msg->pose.pose.position.z);

  _current_odom_position = position_W;

  q.normalize();
  gnomic::Matrix3 q_mat = q.toRotationMatrix();
  _current_yaw_orientation = gnomic::yawFromQuaternion(q);
    
  gnomic::Vector3 velocity_W(q_mat *
                             gnomic::Vector3(odom_msg->twist.twist.linear.x,
                                             odom_msg->twist.twist.linear.y,
                                             odom_msg->twist.twist.linear.z));

  gnomic::Vector3 omega_W(q_mat *
                          gnomic::Vector3(odom_msg->twist.twist.angular.x,
                                          odom_msg->twist.twist.angular.y,
                                          odom_msg->twist.twist.angular.z));

  // check if publisher has been activated
  if(_cmd_pub) {
    if(_target_updated) {
      _dyn_tf = _tf;
      _dyn_N = _N;
      _dyn_N_step_time = _tf/_N;
      _update_target_time = ros::Time::now();
      _target_updated = false;
      _start_posit = position_W;
      _start_yaw = _current_yaw_orientation;
    }
    
    gnomic::real _start_dist = (_start_posit - _solver->getFinalState()->p).norm() + fabs(_start_yaw - gnomic::yawFromQuaternion(_solver->getFinalState()->q)); 
    gnomic::real _curr_dist = (position_W - _solver->getFinalState()->p).norm() + fabs(_current_yaw_orientation - gnomic::yawFromQuaternion(_solver->getFinalState()->q)); 
    _dyn_tf = ( _tf*_curr_dist )/_start_dist;
    
    if(_dyn_tf < 1)
      _dyn_tf = 1;
    
    gnomic::real dt_nominale = _tf/_N;
    //std::cout << dt_nominale << std::endl;
    
    _dyn_N = ceil(_dyn_tf/dt_nominale);
    
    _dyn_N = fmin(_dyn_N, _N);
    _dyn_tf = fmin(_dyn_tf, _tf);
    
    //_solver->setTimeVariables(_dyn_tf, _dyn_N);
  }

  // Initial StateVector
  mav_model::StateVector init_state;
  init_state.p = position_W;
  init_state.q = q;
  init_state.dot_p = velocity_W;
  init_state.omega = omega_W;
  init_state.thrust = -gnomic::gravity * _mass;
  
  // Initial InputVector
  mav_model::InputVector initial_input;
  initial_input.thrust = init_state.thrust;

  _solver->setInitialState(&init_state);
  _solver->setInitialInput(&initial_input);
  
  if(_update_goal) {
    _update_goal = false;
    _solver->init(gnomic::TrajectoryInitialization::LINEAR); 
  }
  else
    _solver->init(gnomic::TrajectoryInitialization::PREVIOUS);

  
  /*
  _solver->compute();

  std::vector<mav_model::Xi> outputTrajectory = _solver->optimalTrajectory();
  
  gnomic::real dt = _solver->dt(0);
    
  mav_model::StateVector state;
  _robot->computeState(state, init_state, outputTrajectory[0],
                       outputTrajectory[1], dt);
  
  mav_model::InputVector input;
  _robot->computeInput(state, input);
  
  mav_msgs::RollPitchYawrateThrust rpyr_cmd;
  gnomic::Vector3 yaw_rate_W(state.q.toRotationMatrix().inverse() *
                             state.omega);
  
  gnomic::real sampling_time = (ros::Time::now() - _prev_time).toSec();
  _prev_time = ros::Time::now();
  
  if (_enable_integrator) {

    gnomic::Vector3 position_error = _solver->getFinalState()->p - position_W;
    gnomic::real angle_error = gnomic::yawFromQuaternion( _solver->getFinalState()->q ) - _current_yaw_orientation;

    if (position_error.norm() < _antiwindup_ball) {
      _position_error_integration += position_error * sampling_time;
    } else {
      _position_error_integration.setZero();
    }
	
    if (fabs(angle_error) < 0.3){
      _angle_damping_factor = - 5*omega_W(2);
    } else {
      _angle_damping_factor = 0.f;
    }
	
    if (fabs(angle_error) < 0.1){
      _angle_error_integration += angle_error * sampling_time;
    } else {
      _angle_error_integration = 0.f;
    }
    
    
    _position_error_integration = _position_error_integration.cwiseMax(
								       gnomic::Vector3(-_position_error_integration_limit,
										       -_position_error_integration_limit,
										       -_position_error_integration_limit));
    
    _position_error_integration = _position_error_integration.cwiseMin(
								       gnomic::Vector3(_position_error_integration_limit, _position_error_integration_limit,
										       _position_error_integration_limit));

    _angle_error_integration = fmin(fmax(_angle_error_integration, -0.15), 0.15);
    
    _integral_cmd = gnomic::Vector3(.005, .005, .2).asDiagonal() *
      _position_error_integration;mav_model::StateVector init_state;
  init_state.p = position_W;
  init_state.q = q;
  init_state.dot_p = velocity_W;
  init_state.omega = omega_W;
  init_state.thrust = -gnomic::gravity * _mass;
  }
    
    
  state.q.normalize();
  
  gnomic::Vector3 feed_forward( state.q.toRotationMatrix().inverse()*state.dot_p);
  
  
  rpyr_cmd.header = odom_msg->header;
  rpyr_cmd.thrust.z = state.thrust + _integral_cmd(2);// + .5*feed_forward(2);
  rpyr_cmd.roll = gnomic::rollFromQuaternion(state.q) + _integral_cmd(0);// + .2*feed_forward(0);
  rpyr_cmd.pitch = gnomic::pitchFromQuaternion(state.q) + _integral_cmd(1);// + .2*feed_forward(1);
  rpyr_cmd.yaw_rate = yaw_rate_W(2) + _angle_damping_factor + .5*_angle_error_integration;
  
  //std::cout << _integral_cmd.transpose() << " " << _angle_error_integration << std::endl;
  //std::cout << "YAW RATE: " << rpyr_cmd.yaw_rate << " DAMPING " << _angle_damping_factor << std::endl; 
  
  _cmd_pub.publish(rpyr_cmd);*/
}

void GnomicNodeMav::computeTrajectory() {
	
	mav_model::StateVector init_state;
	init_state.p = gnomic::Vector3(0,0,0);
	init_state.q = gnomic::Quaternion(1,0,0,0);
	init_state.dot_p = gnomic::Vector3(0,0,0);
	init_state.omega = gnomic::Vector3(0,0,0);
	init_state.thrust = -gnomic::gravity * _mass;
	
	// Initial InputVector
	mav_model::InputVector initial_input;
	initial_input.thrust = init_state.thrust;

	_solver->setInitialState(&init_state);
	_solver->setInitialInput(&initial_input);

	if(_update_goal) {
	_update_goal = false;
	_solver->init(gnomic::TrajectoryInitialization::LINEAR); 
	}
	else
	_solver->init(gnomic::TrajectoryInitialization::PREVIOUS);

	std::cout << '\n';
	std::vector<mav_model::Xi> initializedTrajectory = _solver->optimalTrajectory();
    
	for(unsigned int iter = 0; iter < initializedTrajectory.size(); iter++)
	  initializedTrajectory.at(iter).print();
	
	_solver->compute();

	/*std::vector<mav_model::Xi> outputTrajectory = _solver->optimalTrajectory();
	
	std::cout << '\n';
	std::cout << "Optimal Trajectory: " << std::endl;
	std::cout << '\n';
	
	for(unsigned int iter = 0; iter < outputTrajectory.size(); iter++)
		outputTrajectory[iter].print();*/
	
}
