#include "gnomic_youbot.h"

GnomicNodeYoubot::GnomicNodeYoubot(ros::NodeHandle nh) {
  _loss = 0;
  _robot = 0;
  _solver = 0;

  _nh = nh;
  _tf = 0.f;
  _dyn_tf = 0.f;
  _N = 0;
  _dyn_N = 0;
  _dyn_N_step_time = 0.f;
  _iterations = 0;
  _yaw_rate_factor = 0.f;
  _mass = 0.f;
  _I = gnomic::Matrix3::Identity();
  _prev_time = ros::Time::now();
  _isFirstOdomMsg = true;
  _enable_integrator = true;
  _target_updated = false;
  _update_goal = true;
  _antiwindup_ball = 0.3f;
  _position_error_integration.setZero();
  _position_error_integration_limit = .2f;
  _integral_cmd.setZero();
  _final_state.p = gnomic::Vector2::Zero();
  _final_state.yaw = 0;
  _input_vx_factor, _input_vy_factor,_input_omega_factor = 0.f;
  _angle_error_integration = 0.f;
  
  _current_odom_position.setZero();
  _current_yaw_orientation = 0.f;

  _verbosity = false;
  
}

void GnomicNodeYoubot::initFromYaml(const std::string &yaml_file) {

  // store the yaml filename for possible re-loading
  _yaml_filename = yaml_file;
  
  YAML::Node configuration = YAML::LoadFile(yaml_file);
  std::vector<gnomic::real> inertia_params =
    configuration["dynamic_parameters"]["inertia"].as<std::vector<gnomic::real>>();
  _mass = configuration["dynamic_parameters"]["mass"].as<gnomic::real>();

  /// Init Robot
  _I.diagonal() << inertia_params[0], inertia_params[1], inertia_params[2];
  _robot = new gnomic::YoubotDynamics<youbot_model::InputVector, youbot_model::StateVector, youbot_model::Xi>(_mass, _I);

  /// Init Loss
  _loss = new YoubotLoss();
  _loss->setRobotDynamics(_robot);
  
  /// Init Solver
  _solver = new gnomic::Solver<YoubotLoss>();

  _N = configuration["temporal_parameters"]["N"].as<int>();
  _dyn_N = _N;
  _iterations = configuration["temporal_parameters"]["iterations"].as<int>();
  _tf = configuration["temporal_parameters"]["Tf"].as<gnomic::real>();
  _dyn_tf = _tf;

  _solver->setIterations(_iterations);
  _solver->setTimeVariables(_dyn_tf, _dyn_N);

  /// Setting Weights
  YoubotLoss::Weights weights;
  
  // Final State Weight Matrix 
  _pose_factors = configuration["final_state_factors"]["pose_factors"].as<std::vector<gnomic::real>>();
  _orientation_factors = configuration["final_state_factors"]["orientation_factors"].as<gnomic::real>();
  _linear_velocity_factors = configuration["final_state_factors"]["velocity_factors"].as<std::vector<gnomic::real>>();
  weights.Sf.diagonal() << _pose_factors[0], _pose_factors[1], _orientation_factors, _linear_velocity_factors[0], _linear_velocity_factors[1];
  
  // Dynamics Weight Matrix 
  _pose_factors = configuration["dynamic_state_factors"]["pose_factors"].as<std::vector<gnomic::real>>();
  _orientation_factors = configuration["dynamic_state_factors"]["orientation_factors"].as<gnomic::real>();
  _linear_velocity_factors = configuration["dynamic_state_factors"]["velocity_factors"].as<std::vector<gnomic::real>>();
  weights.Al.diagonal() << _pose_factors[0], _pose_factors[1], _orientation_factors, _linear_velocity_factors[0], _linear_velocity_factors[1];
  
  // Input Weight Matrix
  _input_vx_factor  = configuration["control_input_factors"]["velocity_x_factor"].as<gnomic::real>();
  _input_vy_factor  = configuration["control_input_factors"]["velocity_y_factor"].as<gnomic::real>();
  _input_omega_factor  = configuration["control_input_factors"]["yaw_rate_factor"].as<gnomic::real>();
  weights.R.diagonal() << _input_vx_factor, _input_vy_factor, _input_omega_factor;

  _timeMeshRefinement = configuration["temporal_parameters"]["perform_time_mesh refinement"].as<int>();
  _epsilon = configuration["temporal_parameters"]["discretization_error"].as<gnomic::real>();
  _max_pyramidal_refs = configuration["temporal_parameters"]["max_pyramidal_refinement"].as<int>();
  
  _solver->setTimeMeshRefinement(_timeMeshRefinement, _max_pyramidal_refs, _epsilon);

  _verbosity = configuration["solver_verbosity"]["verbosity"].as<int>();
  _solver->setVerbosity(_verbosity);
  
  _loss->setWeights(weights);
  _solver->setLoss(_loss);

  _solver->setFinalState(&_final_state);
  _target_updated = true;

  std::cerr << "InitFromYAML done!" << std::endl;
  
}

void GnomicNodeYoubot::activateCallback(const std::string &odom_callback_name) {
  _odom_sub = _nh.subscribe(odom_callback_name, 1, &GnomicNodeYoubot::odometryCallback,
                            this, ros::TransportHints().tcpNoDelay());
}

void GnomicNodeYoubot::activateTrajectoryCallback(const std::string &traj_callback_name) {
  _traj_sub = _nh.subscribe(traj_callback_name, 1, &GnomicNodeYoubot::trajectoryCallback,
                            this, ros::TransportHints().tcpNoDelay());
}

void GnomicNodeYoubot::activatePublisher(const std::string &cmd_publisher_name) {
  _cmd_pub =
    _nh.advertise<geometry_msgs::Twist>(cmd_publisher_name, 1);
}

void GnomicNodeYoubot::trajectoryCallback(
                                          const nav_msgs::OdometryConstPtr& traj_msg) {
  
  gnomic::Vector3 position( traj_msg->pose.pose.position.x,
                            traj_msg->pose.pose.position.y,
                            traj_msg->pose.pose.position.z);
  
  gnomic::Quaternion attitude( traj_msg->pose.pose.orientation.w,
                               traj_msg->pose.pose.orientation.x,
                               traj_msg->pose.pose.orientation.y,
                               traj_msg->pose.pose.orientation.z);
    
    
    
  /// Init & Set Final StateVector
  youbot_model::StateVector final_state;
  final_state.p = gnomic::Vector2(position(0), position(1));
  final_state.yaw = gnomic::yawFromQuaternion(attitude);
  final_state.d_p = gnomic::Vector2(traj_msg->twist.twist.linear.x,
                                    traj_msg->twist.twist.linear.y);
  setFinalState(final_state);
      
}


void GnomicNodeYoubot::odometryCallback(
                                        const nav_msgs::OdometryConstPtr &odom_msg) {
  if (_isFirstOdomMsg) {
    _prev_time = ros::Time::now();
    _isFirstOdomMsg = false;
  }
  
  gnomic::Quaternion q(
                       odom_msg->pose.pose.orientation.w, odom_msg->pose.pose.orientation.x,
                       odom_msg->pose.pose.orientation.y, odom_msg->pose.pose.orientation.z);

  gnomic::Vector3 position_W(odom_msg->pose.pose.position.x,
                             odom_msg->pose.pose.position.y,
                             odom_msg->pose.pose.position.z);
  
  gnomic::Vector3 position_B = q.toRotationMatrix().inverse()*position_W;

  _current_odom_position = position_W;

  q.normalize();
  
  gnomic::real yaw = gnomic::yawFromQuaternion(q);
  
  
  gnomic::Matrix3 q_mat = q.toRotationMatrix();
    
  gnomic::Vector3 velocity_B = gnomic::Vector3(odom_msg->twist.twist.linear.x,
                                               odom_msg->twist.twist.linear.y,
                                               odom_msg->twist.twist.linear.z);
  _update_goal = true;
  gnomic::Vector3 velocity_W(q_mat * velocity_B);

  gnomic::real yaw_rate = odom_msg->twist.twist.angular.z;
  
  // check if publisher has been activated
  if(_cmd_pub) {
    if(_target_updated) {
      _dyn_tf = _tf;
      _dyn_N = _N;
      _dyn_N_step_time = _tf/_N;
      _update_target_time = odom_msg->header.stamp;
      _target_updated = false;
      _start_posit = position_W.head(2);
      _start_yaw = yaw;
    }
    
    gnomic::real _start_dist = (_start_posit - _solver->getFinalState()->p).norm() + .5*fabs(_start_yaw - _solver->getFinalState()->yaw ); 
    gnomic::real _curr_dist = (position_W.head(2) - _solver->getFinalState()->p).norm() + .5*fabs(yaw - _solver->getFinalState()->yaw ); 
    _dyn_tf = ( _tf*_curr_dist )/_start_dist;
    if(_dyn_tf < .6)
      _dyn_tf = .6;
    
    gnomic::real dt_nominale = _tf/_N;

    _dyn_N = floor(_dyn_tf/dt_nominale);
    
    _dyn_N = fmin(_dyn_N, _N);
    _dyn_tf = fmin(_dyn_tf, _tf);
    
    std::cout <<  _start_dist << " " << _curr_dist << " DYNTF: " << _dyn_tf << " DYNN: " << _dyn_N << std::endl;

    _solver->setTimeVariables(_dyn_tf, _dyn_N);
  }
  
  // Initial StateVector
  youbot_model::StateVector init_state;
  init_state.p = position_W.block<2,1>(0,0);
  init_state.yaw = yaw;
  init_state.d_p = velocity_B.block<2,1>(0,0);
  init_state.yaw_rate = yaw_rate;

  // Initial InputVector
  youbot_model::InputVector initial_input;
  initial_input.vx = velocity_B(0);
  initial_input.omega = yaw_rate;
  
  _solver->setInitialState(&init_state);
  _solver->setInitialInput(&initial_input);
  if(_update_goal) {
    _update_goal = false;
    _solver->init(gnomic::TrajectoryInitialization::LINEAR); 
  }
  else
    _solver->init(gnomic::TrajectoryInitialization::PREVIOUS);

  gnomic::real dt = _solver->dt(0);
  _solver->compute();

  std::vector<youbot_model::Xi> outputTrajectory = _solver->optimalTrajectory();

  //Find where to pick the input the actual input (according to the _timeMeshRefinement)
  gnomic::real dt_sum = 0;
  int iter = 0;
  while( dt_sum <= dt){
    dt_sum += _solver->dt(iter); 
    iter++; 
  }
  
  youbot_model::StateVector state;
  for(unsigned int i = 0; i < iter; ++i){
    dt = _solver->dt(iter);
    _robot->computeState(state, init_state, outputTrajectory[i],
                         outputTrajectory[i+1], dt);

    // @ Ciro
    // bdc, may the following line is missing??
    init_state = state;
  }


  youbot_model::InputVector input;
  _robot->computeInput(state, input);
 
  
  gnomic::real sampling_time = (ros::Time::now() - _prev_time).toSec();
  _prev_time = ros::Time::now();
  
  if (_enable_integrator) {

    gnomic::Vector2 position_error = _solver->getFinalState()->p - position_W.head(2);
    gnomic::real angle_error = _solver->getFinalState()->yaw - yaw;
   

    if (position_error.norm() < _antiwindup_ball) {
      _position_error_integration += position_error * sampling_time;
      _angle_error_integration += angle_error * sampling_time;
    } else {
      _position_error_integration.setZero();
      _angle_error_integration = 0;
    }
    
    _position_error_integration = _position_error_integration.cwiseMax(
                                                                       gnomic::Vector2(-_position_error_integration_limit,
                                                                                       -_position_error_integration_limit));
    
    _position_error_integration = _position_error_integration.cwiseMin(
                                                                       gnomic::Vector2(_position_error_integration_limit, _position_error_integration_limit));

    _angle_error_integration = fmin(fmax(_angle_error_integration, -0.01), 0.01);
    
    _integral_cmd = gnomic::Vector2(.01, .01).asDiagonal() * _position_error_integration;
  }
  
  gnomic::real angular_feedforward_term(state.yaw_rate);
  gnomic::Vector2 velocity_feedforward_term(state.d_p);
  
  geometry_msgs::Twist twist_cmd;
  
  twist_cmd.angular.z = input.omega + _angle_error_integration + .5*angular_feedforward_term; 
  twist_cmd.linear.x = input.vx + _integral_cmd(0) + .5*velocity_feedforward_term(0); 
  twist_cmd.linear.y = input.vy + _integral_cmd(1) + .5*velocity_feedforward_term(1); 
    
  _cmd_pub.publish(twist_cmd);
}
