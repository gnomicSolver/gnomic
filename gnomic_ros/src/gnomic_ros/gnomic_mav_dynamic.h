#pragma once

#include <iostream>
#include <gnomic/solver.h>
#include <gnomic/loss_dynamic.h>

#include <mav_model_dynamic_tf/mav_dynamics.h>
#include <mav_model_dynamic_tf/input_vector.h>
#include <mav_model_dynamic_tf/state_vector.h>
#include <mav_model_dynamic_tf/xi.h>

#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <mav_msgs/default_topics.h>
#include <mav_msgs/RollPitchYawrateThrust.h>

#include <yaml-cpp/yaml.h>

class GnomicNodeMav{
 public:
  typedef gnomic::Loss<mav_model::InputVector, mav_model::StateVector, mav_model::Xi> MavLoss;
  
  GnomicNodeMav(ros::NodeHandle nh);
    
  ~GnomicNodeMav(){
    std::cerr << "[GnomicNodeMav] deleting\n";
    delete _solver;
    delete _loss;
    delete _robot;      
    std::cerr << "[GnomicNodeMav] deleted\n";
  }

  void initFromYaml(const std::string& yaml_file);
  void trajectoryCallback(const nav_msgs::OdometryConstPtr& traj_msg);
  void odometryCallback(const nav_msgs::OdometryConstPtr& odom_msg);
  void activateCallback(const std::string&);
  void activatePublisher(const std::string&);
  void activateTrajectoryCallback(const std::string&);
  void computeTrajectory();

  void setFinalState(const mav_model::StateVector& final_state){
    _final_state = final_state;
    if(_solver)
      _solver->setFinalState(&_final_state);
    else
      throw std::runtime_error("init the node first!\n");
  }
  void setVerbosity() {
    if(_solver)
      _solver->setVerbosity(true);
    else
      throw std::runtime_error("init the node first!\n");
  }
    
 protected:
 
  MavLoss* _loss;
  gnomic::Solver<MavLoss>* _solver;
  gnomic::BaseRobotDynamics<mav_model::InputVector, mav_model::StateVector, mav_model::Xi>* _robot;

  gnomic::real _dyn_tf;   // trajectory time dynamically adjusted
  int _dyn_N;    // horizon dynamically adjusted
  gnomic::real _dyn_N_step_time;
  ros::Time _update_target_time;
  gnomic::real _tf;       // trajectory time
  int _N;          // horizon
  int _iterations; // max iterations
  gnomic::Vector3 _start_posit;
  gnomic::real _start_yaw;
  bool _target_updated;

  ros::Time _prev_time;
  bool _isFirstOdomMsg;
  bool _enable_integrator;
  bool _update_goal;
  gnomic::real _antiwindup_ball;
  gnomic::Vector3 _position_error_integration;
  gnomic::real _angle_error_integration;
  gnomic::real _angle_damping_factor;
  gnomic::Vector3 _integral_cmd;
  gnomic::real _position_error_integration_limit;

  gnomic::Vector3 _current_odom_position;
  gnomic::real _current_yaw_orientation;
    
  mav_model::StateVector _final_state;
    
  gnomic::Matrix3 _I; //inertia_matrix
  gnomic::real _mass;
    
  std::vector<gnomic::real> _pose_factors;
  std::vector<gnomic::real> _orientation_factors;
  std::vector<gnomic::real> _linear_velocity_factors;
  gnomic::real _yaw_rate_factor;
  gnomic::real _input_tau1_factor, _input_tau2_factor, _input_tau3_factor, _input_thrust_factor;
    
  //node handle
  ros::NodeHandle _nh;

  //subscriber
  ros::Subscriber _odom_sub, _traj_sub;

  //publisher
  ros::Publisher _cmd_pub;

  //param filename
  std::string _yaml_filename;
    
};
