cmake_minimum_required(VERSION 2.8.3)
project(gnomic_ros_gui)

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --std=gnu++11") FLAG PRECEDENTI
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=c++11 -Ofast -march=native") 

set(CMAKE_BUILD_TYPE Release)

message(STATUS "PROCESSOR_TYPE: [${CMAKE_HOST_SYSTEM_PROCESSOR}]")
if (${CMAKE_HOST_SYSTEM_PROCESSOR} MATCHES armv7l)
  set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -march=armv7-a -mfpu=neon-vfpv4 -mfloat-abi=hard -funsafe-math-optimizations")
  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=armv7-a -mfpu=neon-vfpv4 -mfloat-abi=hard -funsafe-math-optimizations")
  message("ENABLING ARM NEON OPTIMIZATIONS")
endif ()



find_package(Eigen3 REQUIRED)

# Find Catkin to manage compilation
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rosbag
  nav_msgs
  sensor_msgs
  mav_msgs
  std_srvs
  gazebo_msgs
  gnomic
  gnomic_ros
)

if(gnomic_FOUND)
  message("GNOMIC Found: " ${gnomic_INCLUDE_DIRS})
else()
  message("GNOMIC NOT Found!")
endif()

if(gnomic_ros_FOUND)
  message("GNOMIC_ROS Found! " ${gnomic_ros_LIBRARIES})
else()
  message("GNOMIC_ROS NOT Found!")
endif()

find_package(glfw3 REQUIRED)
find_package(OpenGL)


catkin_package(
  INCLUDE_DIRS src
  CATKIN_DEPENDS gnomic_ros
  LIBRARIES gnomic_mav_imgui_library gnomic_youbot_imgui_library gnomic_turtlebot_imgui_library
)


include_directories(
  ${catkin_INCLUDE_DIRS}
  ${gnomic_INCLUDE_DIRS}
  ${gnomic_ros_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIRS}
  ${PROJECT_SOURCE_DIRS}/src
  src
)

add_subdirectory(${PROJECT_SOURCE_DIR}/src)
