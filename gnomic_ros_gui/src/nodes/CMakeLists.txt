add_executable(mav_gui_node
  mav_gui_node.cpp
  )
target_link_libraries(mav_gui_node
  gnomic_mav_imgui_library
  )

add_executable(youbot_gui_node
  youbot_gui_node.cpp
  )
target_link_libraries(youbot_gui_node
  gnomic_youbot_imgui_library
  )


add_executable(turtlebot_gui_node
  turtlebot_gui_node.cpp
  )
target_link_libraries(turtlebot_gui_node
  gnomic_turtlebot_imgui_library
  )

