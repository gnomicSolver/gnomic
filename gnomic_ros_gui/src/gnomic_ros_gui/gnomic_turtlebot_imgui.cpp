#include "gnomic_turtlebot_imgui.h"

GnomicTurtlebotGUI::GnomicTurtlebotGUI(ros::NodeHandle nh) : GnomicNodeTurtlebot(nh),
                                                             GnomicBaseGUI(nh) {

  _tf_mode_auto = false;
  _live_update = false;
  
  _des_pos_vec2f[0] = 0.f;
  _des_pos_vec2f[1] = 0.f;
  _des_orientationf = 1.f;

  _antiwindup_ball_float = (float)_antiwindup_ball;
  _tf_float = (float)_tf;
  
  _log_writer = 0;

  _gui_ros_time = ros::Time::now();
  _log_recording = false;

  _log_writer = new std::ofstream();
                                                             }

void GnomicTurtlebotGUI::logOdometryCallback(const nav_msgs::OdometryConstPtr &odom_msg) {
  if(_log_recording) {
    float current_time = (ros::Time::now() - _gui_ros_time).toSec();
  
    Eigen::Quaternionf q(odom_msg->pose.pose.orientation.w,
                         odom_msg->pose.pose.orientation.x,
                         odom_msg->pose.pose.orientation.y,
                         odom_msg->pose.pose.orientation.z);
    q.normalize();
    float yaw = gnomic::yawFromQuaternion(q);
    
    Eigen::Vector3f position_W(odom_msg->pose.pose.position.x,
                               odom_msg->pose.pose.position.y,
                               odom_msg->pose.pose.position.z);

    Eigen::Matrix3f q_mat = q.toRotationMatrix();   
    Eigen::Vector3f velocity_W(q_mat *
                               Eigen::Vector3f(odom_msg->twist.twist.linear.x,
                                               odom_msg->twist.twist.linear.y,
                                               odom_msg->twist.twist.linear.z));

    Eigen::Vector3f omega_W(q_mat *
                            Eigen::Vector3f(odom_msg->twist.twist.angular.x,
                                            odom_msg->twist.twist.angular.y,
                                            odom_msg->twist.twist.angular.z));

    if(_log_writer) {
      *_log_writer << current_time << " "
                   << position_W.x() << " "
                   << position_W.y() << " "
                   << yaw << " "
                   << velocity_W.x() << " "
                   << velocity_W.y() << " "
                   << omega_W.z() << std::endl;     
    }
  }
}



void GnomicTurtlebotGUI::updateFinalState() {
  _final_state.p = gnomic::Vector2( _des_pos_vec2f[0],
                                    _des_pos_vec2f[1]);
  _final_state.yaw = (gnomic::real)_des_orientationf;
  
  _position_error_integration.setZero();
  _target_updated = true;
}

void GnomicTurtlebotGUI::updateRecordState() {
  if(_log_recording) { // turn on
    time(&_timer);  /* get current time; same as: timer = time(NULL)  */
    double seconds = difftime(_timer,mktime(&_y2k));    
    std::string file_full_name = std::string(_log_filename) +"-"+std::to_string(seconds) + ".txt";
    _log_writer->open(file_full_name);
    std::cerr << "[GUI LOG] opened: " << file_full_name << std::endl;
  } else { //turn off
    _log_writer->close();
    std::cerr << "[GUI LOG] closed file" << std::endl;
  }

}

void GnomicTurtlebotGUI::activateLogCallback(const std::string &odom_callback_name) {
  _log_sub = _nh.subscribe(odom_callback_name, 1, &GnomicTurtlebotGUI::logOdometryCallback,
                           this, ros::TransportHints().tcpNoDelay());
}

void GnomicTurtlebotGUI::showGUI(bool *p_open) {

  ImGuiWindowFlags window_flags = 0;
  window_flags |= ImGuiWindowFlags_MenuBar;
 
  ImGui::SetNextWindowSize(ImVec2(550, 680), ImGuiCond_FirstUseEver);
  if (!ImGui::Begin("GnomicTurtlebotGUI", p_open, window_flags)) {
    // Early out if the window is collapsed, as an optimization.
    ImGui::End();
    return;
  }
  
  /// MAIN WINDOW CONTENT
  ImGui::TextColored(ImVec4(0.4f, 0.8f, 0.0f, 1.0f), "GnomicTurtlebotGUI");
  ImGui::Text("Activate callback and publisher with Gazebo simulator open before sending the desired goal.");
  ImGui::Spacing();
  if(ImGui::Button("Gazebo Utils")) _show_gazebo_gui ^= 1;
  ImGui::Spacing(); 
  ImGui::Separator();
  ImGui::Text("Parameters Filename");
  ImGui::Text("%s", _yaml_filename.c_str());
  if(ImGui::Button("Re-Load"))
    initFromYaml(_yaml_filename);
  ImGui::Spacing(); 
  ImGui::Separator();
  ImGui::Text("Callback");
  static char odom_sub[64] = "/odom";
  ImGui::InputText("odom_sub", odom_sub, 64);
  bool activate_callback = ImGui::Button("Activate Callback");
  if (activate_callback) {
    std::cerr << "activating callback: " << odom_sub << std::endl;
    activateLogCallback(odom_sub);
    activateCallback(odom_sub);
  }

  // ros::master::V_TopicInfo master_topics;
  // ros::master::getTopics(master_topics);
  
  // for (ros::master::V_TopicInfo::iterator it = master_topics.begin() ; it != master_topics.end(); it++) {
  //   const ros::master::TopicInfo& info = *it;
  //   std::cout << "Topic : " << it - master_topics.begin() << ": " << info.name << " -> " << info.datatype <<       std::endl;
  // }
  
  // // Test on selecting Topic with filled menu
  // const char* desc[] = 
  //   {
  //     "Resize vertical only",
  //     "Resize horizontal only",
  //   };
  // int type;
  // ImGui::Combo("Topic", &type, _topic_list, IM_ARRAYSIZE(desc));
  
  
  ImGui::Spacing();
  static char cmd_pub[64] = "/cmd_vel_mux/input/navi";
  ImGui::InputText("cmd_pub", cmd_pub, 64);
  bool activate_publisher = ImGui::Button("Activate Publisher");
  if (activate_publisher) {
    std::cerr << "activating publisher: " << cmd_pub << std::endl;
    activatePublisher(cmd_pub);
  }

  ImGui::Spacing();
  ImGui::Separator();
  ImGui::Text("Desired Final State");
  ImGui::Checkbox("Live Update", &_live_update);

  ImGui::Columns(2, "state_time");
  ImGui::DragFloat2("x y [meters]", _des_pos_vec2f, 0.01f, -10.0f, 10.0f);
  ImGui::DragFloat("yaw [radians]", &_des_orientationf, 0.01f, -M_PI, M_PI);
  bool update_final_state = ImGui::Button("Update Final State");
  if (_live_update || update_final_state) {
    updateFinalState();
  }

  ImGui::NextColumn();

  ImGui::Checkbox("TF mode", &_tf_mode_auto);
  ImGui::SameLine(); 
  if(_tf_mode_auto) {
    ImGui::SameLine();
    ImGui::TextColored(ImVec4(1.0f, 1.0f, 0.0f, 1.0f), "auto[TODO]");
  } else {
    ImGui::SameLine();
    ImGui::TextColored(ImVec4(1.0f, 0.0f, 1.0f, 1.0f), "manual");
  }
  ImGui::Text("dynamic [tf: %.3f] [N: %d]", _dyn_tf, _dyn_N);
  ImGui::DragFloat("tf[sec]", &_tf_float, 0.01f, 0.5f, 20.f);

  
  ImGui::DragInt("N", &_N, 1, 3, 20);
  if(ImGui::Button("Update tf/N")) {
    _tf = (gnomic::real)_tf_float;
    _solver->setTimeVariables(_tf, _N);
  }
  
  ImGui::Columns(1);
  

  //add new data for plot and update min_max
  addDataPlot(_x_values, _x_min, _x_max, _current_odom_position(0));
  addDataPlot(_y_values, _y_min, _y_max, _current_odom_position(1));
  addDataPlot(_yaw_values, _yaw_min, _yaw_max, _current_yaw_orientation);

  
  ImGui::Spacing();
  ImGui::Separator();
  ImGui::Text("Current State:");
  ImGui::Columns(3, "mycolumns"); // 3-ways, with border
  ImGui::Separator();
  ImGui::Text("x[m]"); ImGui::NextColumn();
  ImGui::Text("y[m]"); ImGui::NextColumn();
  ImGui::Text("yaw[rad]"); ImGui::NextColumn();
  ImGui::Separator();
  ImGui::Text("%f", _current_odom_position(0)); ImGui::NextColumn();
  ImGui::Text("%f", _current_odom_position(1)); ImGui::NextColumn();
  ImGui::Text("%f", _current_yaw_orientation);  ImGui::NextColumn();
  ImGui::PlotLines("",_x_values, IM_ARRAYSIZE(_x_values), 0,
                   "x", _x_min, _x_max, ImVec2(0,40)); ImGui::NextColumn();
  ImGui::PlotLines("",_y_values, IM_ARRAYSIZE(_y_values), 0,
                   "y", _y_min, _y_max, ImVec2(0,40)); ImGui::NextColumn();
  ImGui::PlotLines("",_yaw_values, IM_ARRAYSIZE(_yaw_values), 0,
                   "yaw", _yaw_min, _yaw_max, ImVec2(0,40));
  ImGui::Columns(1);

  
  ImGui::Spacing();
  ImGui::Separator();
  ImGui::Checkbox("Integral Action", &_enable_integrator);
  if (_enable_integrator) {
    ImGui::SameLine();
    ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "ON");
  } else {
    ImGui::SameLine();
    ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "OFF");
  }

  ImGui::DragFloat("antiwindup_ball", &_antiwindup_ball_float, 0.01f, -10.0f, 10.0f);
  // TOTO ADD BUTTON
  
  ImGui::Spacing();
  ImGui::Separator();

  ImGui::Text("Pose/Velocity Log");
  ImGui::Spacing();
  ImGui::InputText("-[time].txt", _log_filename, 100);
  if(ImGui::Button("Rec/Stop")) {
    _log_recording ^= 1;
    updateRecordState();
  }
  if (_log_recording) {
    ImGui::SameLine();
    ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Rec");
  } else {
    ImGui::SameLine();
    ImGui::TextColored(ImVec4(0.0f, 1.0f, 1.0f, 1.0f), "Stop");
  }
    
  // Show Here Auxiliar GUIs
  if(_show_gazebo_gui) {
    showGazeboGUI(&_show_gazebo_gui);
  }

    
}
