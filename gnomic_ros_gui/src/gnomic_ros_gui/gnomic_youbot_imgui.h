#pragma once

#include <iostream>
#include <fstream>
#include <time.h>
#include <gnomic_ros/gnomic_youbot.h>
#include <gazebo_msgs/SetModelState.h>
#include <std_srvs/Empty.h>

#include "gnomic_base_imgui.h"


class GnomicYoubotGUI: public GnomicNodeYoubot, public GnomicBaseGUI{
public:
    GnomicYoubotGUI(ros::NodeHandle nh);
    
    ~GnomicYoubotGUI() {
	std::cerr << "[GnomicYoubotGUI]: deleting\n";
	delete _log_writer;
	std::cerr << "[GnomicYoubotGUI]: deleted\n";
    }

    void showGUI(bool* p_open);

private:

    void logOdometryCallback(const nav_msgs::OdometryConstPtr& odom_msg);
    char _log_filename[100] = "gnomic_youbot_log";
    bool _log_recording;
    std::ofstream *_log_writer;
    ros::Time _gui_ros_time;
    ros::Subscriber _log_sub;
    void activateLogCallback(const std::string &odom_callback_name);

    void updateFinalState();
    void updateRecordState();

    //Gui utils
    float _des_pos_vec2f[2];
    float _des_orientationf;
    bool _live_update;
    float _x_values[PLOT_LINE_ARRAY_SIZE] = {0};
    float _x_min = 0;
    float _x_max = 0;
    float _y_values[PLOT_LINE_ARRAY_SIZE] = {0};
    float _y_min = 0;
    float _y_max = 0;
    float _z_values[PLOT_LINE_ARRAY_SIZE] = {0};
    float _z_min = 0;
    float _z_max = 0;
    float _yaw_values[PLOT_LINE_ARRAY_SIZE] = {0};
    float _yaw_min = 0;
    float _yaw_max = 0;
    bool _tf_mode_auto;

};
