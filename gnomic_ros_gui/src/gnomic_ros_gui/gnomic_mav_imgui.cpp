#include "gnomic_mav_imgui.h"

GnomicMavGUI::GnomicMavGUI(ros::NodeHandle nh) : GnomicNodeMav(nh), GnomicBaseGUI(nh) {

  _tf_mode_auto = false;
  _live_update = false;
  _trajectory_following = false;
  
  _des_pos_vec3f[0] = 2.f;
  _des_pos_vec3f[1] = 2.f;
  _des_pos_vec3f[2] = 1.f;
  _des_orientationf = 1.57;


  _antiwindup_ball_float = (float)_antiwindup_ball;
  _tf_float = (float)_tf;
  
  _log_writer = 0;

  _gui_ros_time = ros::Time::now();
  _log_recording = false;

  _log_writer = new std::ofstream();
}

void GnomicMavGUI::logOdometryCallback(const nav_msgs::OdometryConstPtr &odom_msg) {
  _tf_float = (float)_tf;
  if(_log_recording) {
    float current_time = (ros::Time::now() - _gui_ros_time).toSec();
  
    Eigen::Quaternionf q(odom_msg->pose.pose.orientation.w,
                         odom_msg->pose.pose.orientation.x,
                         odom_msg->pose.pose.orientation.y,
                         odom_msg->pose.pose.orientation.z);

    Eigen::Vector3f position_W(odom_msg->pose.pose.position.x,
                               odom_msg->pose.pose.position.y,
                               odom_msg->pose.pose.position.z);

    q.normalize();
    Eigen::Matrix3f q_mat = q.toRotationMatrix();
    
    Eigen::Vector3f velocity_W(q_mat *
                               Eigen::Vector3f(odom_msg->twist.twist.linear.x,
                                               odom_msg->twist.twist.linear.y,
                                               odom_msg->twist.twist.linear.z));

    Eigen::Vector3f omega_W(q_mat *
                            Eigen::Vector3f(odom_msg->twist.twist.angular.x,
                                            odom_msg->twist.twist.angular.y,
                                            odom_msg->twist.twist.angular.z));

    if(_log_writer) {
      *_log_writer << current_time << " "
                   << position_W.x() << " "
                   << position_W.y() << " "
                   << position_W.z() << " "
                   << q.w() << " "
                   << q.x() << " "
                   << q.y() << " "
                   << q.z() << " "
                   << velocity_W.x() << " "
                   << velocity_W.y() << " "
                   << velocity_W.z() << " "
                   << omega_W.x() << " "
                   << omega_W.y() << " "
                   << omega_W.z() << std::endl;     
    }
  }
}



void GnomicMavGUI::updateFinalState() {
  _final_state.p = gnomic::Vector3( _des_pos_vec3f[0],
                                    _des_pos_vec3f[1],
                                    _des_pos_vec3f[2]);
  _final_state.q = gnomic::AngleAxis(_des_orientationf, gnomic::Vector3::UnitZ() );
  
  _position_error_integration.setZero();
  _target_updated = true;
}

void GnomicMavGUI::updateRecordState() {
  if(_log_recording) { // turn on
    time(&_timer);  /* get current time; same as: timer = time(NULL)  */
    double seconds = difftime(_timer,mktime(&_y2k));    
    std::string file_full_name = std::string(_log_filename) +"-"+std::to_string(seconds) + ".txt";
    _log_writer->open(file_full_name);
    std::cerr << "[GUI LOG] opened: " << file_full_name << std::endl;
  } else { //turn off
    _log_writer->close();
    std::cerr << "[GUI LOG] closed file" << std::endl;
  }

}

void GnomicMavGUI::activateLogCallback(const std::string &odom_callback_name) {
  _log_sub = _nh.subscribe(odom_callback_name, 1, &GnomicMavGUI::logOdometryCallback,
                           this, ros::TransportHints().tcpNoDelay());
}


void GnomicMavGUI::showGUI(bool *p_open) {

  ImGuiWindowFlags window_flags = 0;
  window_flags |= ImGuiWindowFlags_MenuBar;
 
  ImGui::SetNextWindowSize(ImVec2(550, 680), ImGuiCond_FirstUseEver);
  if (!ImGui::Begin("GnomicMavGUI", p_open, window_flags)) {
    // Early out if the window is collapsed, as an optimization.
    ImGui::End();
    return;
  }
  
  /// MAIN WINDOW CONTENT
  ImGui::TextColored(ImVec4(0.4f, 0.8f, 0.0f, 1.0f), "GnomicMavGUI");
  ImGui::Text("Activate callback and publisher with Gazebo simulator open before sending the desired goal.");
  ImGui::Spacing();
  if(ImGui::Button("Gazebo Utils")) _show_gazebo_gui ^= 1;
  ImGui::Spacing(); 
  ImGui::Separator();
  ImGui::Text("Parameters Filename");
  ImGui::Text("%s", _yaml_filename.c_str());
  if(ImGui::Button("Re-Load"))
    initFromYaml(_yaml_filename);
  ImGui::Spacing(); 
  ImGui::Separator();
  ImGui::Text("Callback");
  static char odom_sub[64] = "/firefly/ground_truth/odometry";
  ImGui::InputText("odom_sub", odom_sub, 64);
  bool activate_callback = ImGui::Button("Activate Callback");
  if (activate_callback) {
    std::cerr << "activating callback: " << odom_sub << std::endl;
    activateLogCallback(odom_sub);
    activateCallback(odom_sub);
  }

  ImGui::Spacing();
  static char cmd_pub[64] = "/command/roll_pitch_yawrate_thrust";
  ImGui::InputText("cmd_pub", cmd_pub, 64);
  bool activate_publisher = ImGui::Button("Activate Publisher");
  if (activate_publisher) {
    std::cerr << "activating publisher: " << cmd_pub << std::endl;
    activatePublisher(cmd_pub);
  }

  ImGui::Spacing();
  ImGui::Separator();
  ImGui::Text("Desired Final State");
  ImGui::Checkbox("Live Update", &_live_update);
  
  ImGui::Columns(2, "state_time");
  ImGui::DragFloat3("x y z [meters]", _des_pos_vec3f, 0.01f, -10.0f, 10.0f);
  ImGui::DragFloat("yaw [radians]", &_des_orientationf, 0.01f, -M_PI, M_PI);
  bool update_final_state = ImGui::Button("Update Final State");
  if (_live_update || update_final_state) {
    updateFinalState();
  }

  ImGui::NextColumn();

  ImGui::Checkbox("TF mode", &_tf_mode_auto);
  ImGui::SameLine(); 
  if(_tf_mode_auto) {
    ImGui::SameLine();
    ImGui::TextColored(ImVec4(1.0f, 1.0f, 0.0f, 1.0f), "auto[TODO]");
  } else {
    ImGui::SameLine();
    ImGui::TextColored(ImVec4(1.0f, 0.0f, 1.0f, 1.0f), "manual");
  }
  ImGui::Text("dynamic [tf: %.3f] [N: %d]", _dyn_tf, _dyn_N);
  ImGui::DragFloat("tf[sec]", &_tf_float, 0.01f, 0.5f, 20.f);
  
  ImGui::DragInt("N", &_N, 1, 3, 20);
  if(ImGui::Button("Update tf/N")) {
    _tf = (gnomic::real)_tf_float;
    _solver->setTimeVariables(_tf, _N);
  }
  
  ImGui::Columns(1);
  

  //add new data for plot and update min_max
  addDataPlot(_x_values, _x_min, _x_max, _current_odom_position(0));
  addDataPlot(_y_values, _y_min, _y_max, _current_odom_position(1));
  addDataPlot(_z_values, _z_min, _z_max, _current_odom_position(2));
  addDataPlot(_yaw_values, _yaw_min, _yaw_max, _current_yaw_orientation);

  static char traj_topic[64] = "/command/pose";
  ImGui::InputText("trajectory_sub", traj_topic, 64);
  if(ImGui::Button("Activate Trajectory Callback")){
    _trajectory_following = true;
    activateTrajectoryCallback(traj_topic);
  }  
  ImGui::Spacing();
  ImGui::Separator();
  ImGui::Text("Current State:");
  ImGui::Columns(4, "mycolumns"); // 4-ways, with border
  ImGui::Separator();
  ImGui::Text("x[m]"); ImGui::NextColumn();
  ImGui::Text("y[m]"); ImGui::NextColumn();
  ImGui::Text("z[m]"); ImGui::NextColumn();
  ImGui::Text("yaw[rad]"); ImGui::NextColumn();
  ImGui::Separator();
  ImGui::Text("%f", _current_odom_position(0)); ImGui::NextColumn();
  ImGui::Text("%f", _current_odom_position(1)); ImGui::NextColumn();
  ImGui::Text("%f", _current_odom_position(2)); ImGui::NextColumn();
  ImGui::Text("%f", _current_yaw_orientation);  ImGui::NextColumn();

  if(_trajectory_following) {
    _target_updated = true;
    addDataPlot(_des_x_values, _x_min, _x_max, _final_state.p.x());
    addDataPlot(_des_y_values, _y_min, _y_max, _final_state.p.y());
    addDataPlot(_des_z_values, _z_min, _z_max, _final_state.p.z());
    addDataPlot(_des_yaw_values, _yaw_min, _yaw_max, gnomic::yawFromQuaternion(_final_state.q));

    /*TODO: init only ones */
    const int size = 2;
    const char* desired = "desired";
    const char* current = "current";
    const char* names[size];
    names[0] = current;
    names[1] = desired;
    const float* x_data_pair[size];
    x_data_pair[0] = _x_values;
    x_data_pair[1] = _des_x_values;
    const float* y_data_pair[size];
    y_data_pair[0] = _y_values;
    y_data_pair[1] = _des_y_values;
    const float* z_data_pair[size];
    z_data_pair[0] = _z_values;
    z_data_pair[1] = _des_z_values;
    const float* yaw_data_pair[size];
    yaw_data_pair[0] = _yaw_values;
    yaw_data_pair[1] = _des_yaw_values;
    ImColor red = ImColor(255,0,0);
    ImColor green = ImColor(0,255,0);
    ImColor colors[2];
    colors[0] = green; 
    colors[1] = red;
    
    ImGui::PlotMultiLines("",size,names,colors,x_data_pair,IM_ARRAYSIZE(_x_values),_x_min,_x_max,ImVec2(0,40)); ImGui::NextColumn();
    ImGui::PlotMultiLines("",size,names,colors,y_data_pair,IM_ARRAYSIZE(_x_values),_y_min,_y_max,ImVec2(0,40)); ImGui::NextColumn();
    ImGui::PlotMultiLines("",size,names,colors,z_data_pair,IM_ARRAYSIZE(_x_values),_z_min,_z_max,ImVec2(0,40)); ImGui::NextColumn();
    ImGui::PlotMultiLines("",size,names,colors,yaw_data_pair,IM_ARRAYSIZE(_x_values),_yaw_min,_yaw_max,ImVec2(0,40));
    
  } else {
    ImGui::PlotLines("",_x_values, IM_ARRAYSIZE(_x_values), 0,
                     "x", _x_min, _x_max, ImVec2(0,40)); ImGui::NextColumn();
    ImGui::PlotLines("",_y_values, IM_ARRAYSIZE(_y_values), 0,
                     "y", _y_min, _y_max, ImVec2(0,40)); ImGui::NextColumn();
    ImGui::PlotLines("",_z_values, IM_ARRAYSIZE(_z_values), 0,
                     "z", _z_min, _z_max, ImVec2(0,40)); ImGui::NextColumn();
    ImGui::PlotLines("",_yaw_values, IM_ARRAYSIZE(_yaw_values), 0,
                     "yaw", _yaw_min, _yaw_max, ImVec2(0,40));
  }
  ImGui::Columns(1);
  
  ImGui::Spacing();
  ImGui::Separator();
  ImGui::Checkbox("Integral Action", &_enable_integrator);
  if (_enable_integrator) {
    ImGui::SameLine();
    ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "ON");
  } else {
    ImGui::SameLine();
    ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "OFF");
  }

  ImGui::DragFloat("antiwindup_ball", &_antiwindup_ball_float, 0.01f, -10.0f, 10.0f);
  // TODO ADD BUTTON TO SET IT

  ImGui::Spacing();
  ImGui::Separator();

  ImGui::Text("Pose/Velocity Log");
  ImGui::Spacing();
  ImGui::InputText("-[time].txt", _log_filename, 100);
  if(ImGui::Button("Rec/Stop")) {
    _log_recording ^= 1;
    updateRecordState();
  }
  if (_log_recording) {
    ImGui::SameLine();
    ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Rec");
  } else {
    ImGui::SameLine();
    ImGui::TextColored(ImVec4(0.0f, 1.0f, 1.0f, 1.0f), "Stop");
  }
    
  // Show Here Auxiliar GUIs
  if(_show_gazebo_gui) {
    showGazeboGUI(&_show_gazebo_gui);
  }
   
}

