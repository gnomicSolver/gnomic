#pragma once

#include <iostream>
#include <fstream>
#include <time.h>
#include <gnomic_ros/gnomic_mav.h>
#include <gazebo_msgs/SetModelState.h>
#include <std_srvs/Empty.h>

#include "gnomic_base_imgui.h"


class GnomicMavGUI: public GnomicNodeMav, public GnomicBaseGUI{
public:
    GnomicMavGUI(ros::NodeHandle nh);
    
    ~GnomicMavGUI() {
	std::cerr << "[GnomicMavGUI]: deleting\n";
	delete _log_writer;
	std::cerr << "[GnomicMavGUI]: deleted\n";
    }

    void showGUI(bool* p_open);

private:

    void logOdometryCallback(const nav_msgs::OdometryConstPtr& odom_msg);
    char _log_filename[100] = "gnomic_mav_log";
    bool _log_recording;
    std::ofstream *_log_writer;
    ros::Time _gui_ros_time;
    ros::Subscriber _log_sub;
    void activateLogCallback(const std::string &odom_callback_name);

    
    void updateFinalState();
    void updateRecordState();

    //Gui utils
    float _des_pos_vec3f[3];
    float _des_orientationf;
    bool _live_update;
    bool _trajectory_following;
    float _x_values[PLOT_LINE_ARRAY_SIZE] = {0};
    float _des_x_values[PLOT_LINE_ARRAY_SIZE] = {0};
    float _x_min = 0;
    float _x_max = 0;
    float _y_values[PLOT_LINE_ARRAY_SIZE] = {0};
    float _des_y_values[PLOT_LINE_ARRAY_SIZE] = {0};
    float _y_min = 0;
    float _y_max = 0;
    float _z_values[PLOT_LINE_ARRAY_SIZE] = {0};
    float _des_z_values[PLOT_LINE_ARRAY_SIZE] = {0};
    float _z_min = 0;
    float _z_max = 0;
    float _yaw_values[PLOT_LINE_ARRAY_SIZE] = {0};
    float _des_yaw_values[PLOT_LINE_ARRAY_SIZE] = {0};
    float _yaw_min = 0;
    float _yaw_max = 0;
    bool _tf_mode_auto;

};
