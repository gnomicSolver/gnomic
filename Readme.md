# GNOMIC #
__GNOMIC__: _Generic NOnlinear Model predIctive Control_.

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/2d80b593296c4f9faffeed2c0ddcacde)](https://www.codacy.com?utm_source=git@bitbucket.org&amp;utm_medium=referral&amp;utm_content=cirpote/gnomic&amp;utm_campaign=Badge_Grade)

## Prerequisites

### Test Environment
Install the following packages in ros Kinetic (with Ubuntu 16.04)

Check the Readme(s) of the packages for futher dependencies

#### MAV environment

* [mav_comm](https://github.com/bartville/mav_comm)

* [rotors_simulator](https://github.com/bartville/rotors_simulator)

#### Youbot environment

* [Youbot World](http://www.youbot-store.com/wiki/index.php?title=Gazebo_simulation&hmswSSOID=10b4d7be36c130126e02a9c81ce579a7f71c954f)

#### Turtlebot environment

* `sudo apt-get install ros-kinetic-turtlebot-gazebo`

### get GNOMIC Software

* `sudo apt install libglfw3-dev`  # needed for the GUIs
* `git clone https://bitbucket.org/gnomicsolver/gnomic.git`

## Authors

* Ciro Potena
* Bartolomeo Della Corte