#pragma once
#include <gnomic/utils.hpp>
#include <iostream>

namespace gnomic {
  
  /**
   * Base struct for a FlatState, it has to be inherited.
   * When inheriting:
   *      - the static size has to be re-defined
   *      - the ptr method has to be overridden
   *      - the update method has to be overridden
   *      - the operator- has to be re-defined
   *      - the operator+ has to be re-defined
   *      - the operator* has to be re-defined
   *      - the operator= has to be re-defined
   *      - the print method has to be overridden   
   */
  struct BaseFlatState {

    /** 
     * This static variable stores the BaseFlatState size.
     * It is used in the Solver to resize matrices at compile time.
     * It has to be re-defined in the derived struct(s)
     */
    const static int size = -1;

    /**
     * Pointer to the first element of the struct.
     * It is used to perform arithmetic-pointer access to struct's member.
     * It has to be overridden by inherited class
     * @return The pointer to the first element of the struct.
     */
    virtual real* ptr() = 0;

    /**
     * Performs the update of the FlatState, given an Eigen::Vector<>.
     * The Vector contains the delta-solution of the NMPC solver.
     * It has to be overridden by inherited class.
     * @param v update vector for this FlatState 
     */
    template<typename Derived>
    void update(const Eigen::MatrixBase<Derived>& v) {
      throw std::runtime_error("[GNOMIC][BaseFlatState]: you should override update() method in your derived Flat State!");
    }

    /**
     * Allows the copy operation between BaseFlatState (s).
     * It has to be overridden by inherited class
     * @param a the right value of the operator=
     */
    BaseFlatState &operator=(const BaseFlatState &a) {
      throw std::runtime_error("[GNOMIC][BaseFlatState]: you should override operators in your derived Flat State!");
    }
      
    /**
     * Allows the sum operation between BaseFlatState (s).
     * It has to be overridden by inherited class
     * @param a the right value of the operator+
     */
      BaseFlatState &operator+(const BaseFlatState &a) const {
        throw std::runtime_error("[GNOMIC][BaseFlatState]: you should override operators in your derived Flat State!");
      }

    /**
     * Allows the dot product operation between a BaseFlatState and a float value.
     * It has to be overridden by inherited class
     * @param a the right value of the operator*
     */
    BaseFlatState &operator*(const real &a) const {
      throw std::runtime_error("[GNOMIC][BaseFlatState]: you should override operators in your derived Flat State!");
    }

    /**
     * Allows the minus operation between a BaseFlatState (s).
     * It has to be overridden by inherited class
     * @param a the right value of the operator-
     */
    BaseFlatState &operator-(const BaseFlatState &a) const {
      throw std::runtime_error("[GNOMIC][BaseFlatState]: you should override operators in your derived Flat State!");
    }

    void unNormalize(const BaseFlatState&a) {
      throw std::runtime_error("[GNOMIC][BaseFlatState]: you should override unNormalize method in your derived Flat State!");     
    }
    
    /**
     * Prints in terminal the content of the BaseFlatState
     * It has to be overridden by inherited class    
     */
    virtual void print() const = 0;
  };
}
