#pragma once
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Geometry>

#include "base_flat_state.h"
#include "base_state_vector.h"
#include "base_input_vector.h"
#include "base_loss.h"

#include <type_traits>
#include <chrono>
#include <float.h>
#include "profiler.h"
#include "colors.h"

namespace gnomic {

  template<typename Loss, typename Enable = void>
    class Solver;
  /**
   * The solver is the core of gnomic. It solves the NMPC problem.
   * It's a template class <Loss<Input, State, FlatState>>.
   * When instantiating an object Solver:
   *      - the Loss<Input, State, FlatState> has to be of type BaseLoss<Input, State, FlatState>, then:
   *             - Input has to be of type BaseInputVector
   *             - State has to be of type BaseStateVector
   *             - FlatState has to be of type BaseFlatState
   * Example:
   *
   *   typedef Loss<myRobot::Input, myRobot::State, myRobot::FlatState> MyLoss;
   *   Solver<MyLoss> my_solver;
   *
   * Once instantiated, the Solver can be used as:
   *
   *   my_solver.setLoss(my_loss);
   *   my_solver.setInitialInput(&initial_input);
   *   my_solver.setInitialState(&initial_state);
   *   my_solver.setFinalState(&final_state);
   *   my_solver.setIterations(it_number);
   *   my_solver.setTimeVariables(tf, N);
   *   my_solver.init(TrajectoryInitialization::LINEAR);
   *   my_solver.compute();
   *   solution = my_solver.optimalTrajectory();
   */
  template< template <typename, typename, typename > typename Loss, typename Input, typename State, typename FlatState>
    class Solver<Loss<Input, State, FlatState>, typename std::enable_if<std::is_base_of<BaseLoss<Input, State, FlatState>, Loss<Input, State, FlatState>>::value>::type>
    {


      /**
       * The size of the input is recovered directly from the Input type
       */
      const static int _input_size       = Input::size;
      /**
       * The size of the state is recovered directly from the State type
       */
      const static int _state_size       = State::size;
      /**
       * The size of the input is recovered directly from the FlatState type
       */
      const static int _flat_size       = FlatState::size;
      /**
       * The size of the error is recovered directly from the Loss type
       */
      const static int _constraints_size = State::_constraints_size;
      /**
       * The size of the error is recovered directly from the Loss type
       */
      const static int _total_error_size = Loss<Input, State, FlatState>::size;
      /**
       * typedefs
       */
      typedef Eigen::Matrix<real, _total_error_size, 1> ErrorVector;


    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

      /**
       * Constructor.
       * It initializes all the members of the Solver.
       */
      Solver();

      /**
       * Destructor
       */
      ~Solver(){
        std::cerr << FRED("[Solver] deleting\n");
        std::cerr << BOLD(FRED("[Solver] deleted\n"));
      }

      /**
       * Returns the optimalTrajectory, i.e. the NMPC solution
       * @return a vector of FlatState, i.e. the NMPC solution
       */
      std::vector<FlatState> optimalTrajectory(){
        return _final_optimal_trajectory;
      }
      
      /**
       * Returns the solution state at a certain time, 
       * @param time at which the solution is required
       * @return State, part of the NMPC solution, at time t
       */
      State optimalTrajectory(const real& t_);
      
      /**
       * Sets the Loss function
       * @param loss pointer to a loss object
       */
      inline void setLoss(Loss<Input, State, FlatState> *loss) {
        _loss = loss;
      }

      /**
       * Sets the Time variables and resizes consequently _dt_vec.
       * In particular initializes a vector
       * of delta_time(s) that may vary for each horizon step
       * @param tf
       * @param N
       */
      inline void setTimeVariables(const real& tf, const int N){
        _tf = tf;
        _N = N;
        // resize dt_vec consequently
        _dt_vec = std::vector<real>(_N, _tf/_N);
      }

      /**
       * Sets the initial input pointer.
       * @param initial_input pointer to initial Input
       */
      inline void setInitialInput(Input* initial_input){
        _initial_input = initial_input;
      }
    
      /**
       * Sets the variables for performing the iterative time mesh refinement.
       * @param time_mesh_refinement boolean for performing the iterative time mesh refinement
       * @param max_pyramidal_refs max number of iterative pyramidal refinements over the time mesh
       * @param epsilon discretization error threshold for stopping the iterative refinement
       */
      inline void setTimeMeshRefinement(const bool time_mesh_refinement,
                                        const int max_pyramidal_refs,
                                        const real& tm_epsilon_){
        _time_mesh_refinement = time_mesh_refinement;
        _max_pyramidal_refs = max_pyramidal_refs;
        if(_max_pyramidal_refs > 2)
          throw std::runtime_error("[SOLVER][setTimeMeshRefinement]: pyramids > 2 not supported yet!");
        
        _tm_epsilon = tm_epsilon_;
      }

 
      /**
       * Sets the initial state pointer.
       * @param initial_state pointer to initial State
       */
      inline void setInitialState(State* initial_state) {
        _initial_state = initial_state;
      }

      /**
       * Initialize all matrices required during the optimization procedure
       */
      void initialize_matrices(const int error_dim_,
                               const int params_dim_,
                               const int N_);
    
      /**
       * Sets the final state pointer.
       * @param final_state pointer to the final State
       */
      inline void setFinalState(State* final_state) {
        _final_state = final_state;
        _backup_final_state = final_state;
      }

      /**
       * Gets the final state pointer.
       * @return pointer to the final State
       */
      inline State* getFinalState(){
        return _final_state;
      }

      /**
       * Sets the Least Squares Iteration
       * @param it least squares iterations
       */
      inline void setIterations(int it, bool
                                perform_all_iterations = false){
        _iterations = it;
        _perform_all_iterations = perform_all_iterations;
      }

      /**
       * Sets the solver verbosity
       * @param verbose boolean to set the Solver verbosity
       */
      inline void setVerbosity(bool verbose){
        _verbose = verbose;
      }

      /**
       * Returns the i-th dt
       * @param i
       * @return value of _dt_vec[i]
       */
      inline real dt(int i){
        if( i >= _dt_vec.size())
          throw std::runtime_error("[SOLVER][dt]: accessing out of _dt_vec size!");
        return _dt_vec[i];
      }

      /**
       * Returns the current tf
       * @return value of _tf
       */
      inline real tf(){
        return _tf;
      }

      /**
       * Returns the current N
       * @return value of _N
       */
      inline int N(){
        return _N;
      }

      /**
       * Returns the profiler with time stats
       * @return const ref to profiler
       */
      const Profiler& profiler() const {return *_profiler;}
      
      /**
       * Inits the solver by dimensioning the least-squares matrices,
       * and the timing and trajectory related variables
       * @param initFlag a flag of kind TrajectoryInitialization
       */
      void init(const TrajectoryInitialization& initFlag);

      /**
       * Invokes the least squares solver.
       */
      void compute();


    private:

      /**
       * Perform the time mesh refinement in the time grid across
       * the actual control snapshot
       */
      std::vector<FlatState> timeMeshRefinement();
    
      /* /\** */
      /*  * Check where there is the need to actually perform a timeMeshRefinement */
      /*  *\/ */
      /* void discretizationErrorCheck( const int iter, */
      /*                                std::vector<real>& discretization_error, */
      /*                                std::vector<bool>& need_refinement,  */
      /*                                const std::vector<int>& coarse_snapshot_indexes, */
      /*                                const std::vector<real>& dt_vec_, */
      /*                                const std::vector<FlatState>& optimal_trajectory_); */


      void discretizationErrorCheck(real& discretization_error,
                                    const real& dt,
                                    const State& state,
                                    const State& next_state,
                                    const FlatState& trajectory_flat_state,
                                    const FlatState& next_trajectory_flat_state);
      

      /**
       * Initializes the timing members and the optimal trajectory.
       * This method is invoked by the init() method.
       * @param initFlag a flag of kind TrajectoryInitialization
       */
      void initializeOptimalTrajectory(const TrajectoryInitialization& initFlag);

      /**
       * Performs one iterations of the least squares problem.
       * This method is invoked by the compute()
       */
      void linearize(const int N_,
                     const std::vector<real>& dt_vec_,
                     const std::vector<FlatState>& optimal_trajectory_,
                     const State& final_state_);

      /**
       * Updates the FlatState (s) that constitute the solution
       * by applying the delta correction, i.e. the solution of the least squares
       */
      void updateXi(std::vector<FlatState>& trajectory_,
                    const VectorX& delta_x_,
                    const int parameters_dim_);

      /**
       * Solver verbosity. Default: false
       */
      bool _verbose;

      /**
       * Least Squares Iterations
       */
      int _iterations;

      /**
       * if true, no termination criteria are used
       */
      bool _perform_all_iterations;
      
      /**
       * Least Squares approximation of Hessian Matrix: H += J' * J
       */
      MatrixX _H;

      /**
       * Least Squares residual Vector: b += J' * e
       */
      VectorX _b;

      /**
       * Least Squares error Vector
       */
      VectorX _error;

      /**
       * Least Squares Jacobian
       */
      MatrixX _J;

      /**
       * Least Squares correction Vector.
       *  _delta_x = -_H\_b (in octave style)
       */
      VectorX _delta_x;

      /**
       * Pointer to Loss. Usually should be BaseLoss,
       * but given the Loss template
       * where Loss is forced to be a BaseLoss child, we can
       * directly use Loss
       */
      Loss<Input, State, FlatState> *_loss;

      /**
       * Pointer to initial State
       */
      State *_initial_state;

      /**
       * Pointer to final State
       */
      State *_final_state;

      /**
       * Backup to final State
       */
      State *_backup_final_state;
    
      /**
       * Pointer to initial Input
       */
      Input *_initial_input;

      /**
       * the solution of the NMPC problem
       * expressed as a vector of FlatState
       */
      std::vector<FlatState> _optimal_trajectory;
      std::vector<FlatState> _best_optimal_trajectory;
      std::vector<FlatState> _final_optimal_trajectory;

      /**
       * Final Time for trajectory execution
       */
      real _tf;

      /**
       * Horizon
       */
      real _N;
    
      /**
       * Backup Horizon
       */
      real _backup_N;
    
      /**
       * Discretization Error for Time Mesh Refinement
       */
      real _tm_epsilon;
    
      /**
       * Max number of pyramidal refinements
       */
      real _max_pyramidal_refs;

      /**
       * Vector of deltaT between horizon steps 
       */
      std::vector<real> _dt_vec;
    
      /**
       * Full error dimension: Loss::size * (_N-1)
       */
      int _error_dim;

      /**
       * Full parameter dimension: FlatState::size * (_N-1)
       */
      int _parameters_dim;

      /**
       * termination criteria variables
       */
      real _chi2_prev;

      /**
       * size of the hysteresis window
       */
      int _hysteresis_size;

      /**
       * use or not the timeMeshRefinement
       */
      bool _time_mesh_refinement;

    
      /**
       * vector of size _hysteresis_size
       */
      std::vector<bool> _higher_chi;

      /**
       * vector of size N, stores the pairs <State, Input>
       */
      std::vector< std::pair<State, Input>,
        Eigen::aligned_allocator<std::pair<State, Input> >
        > _backup_initials;
    
      /**
       * vector that stores the pairs of <State, Input> 
       * during the time mesh refinement procedure
       */
      std::vector< std::pair<State, Input>,
        Eigen::aligned_allocator<std::pair<State, Input> > 
        > _backup_time_mesh_refinement;
      
      /**
       * vector that stores the _optimal_trajectory while
       * performing the time mesh refinement 
       */
      std::vector<FlatState> _backup_optimal_trajectory;	
    
      /**
       * vector that stores the _dt_vec while
       * performing the time mesh refinement
       */
      std::vector<real> _backup_dt_vec;
		 
      /**
       * vector that stores in memory the interpolated FlatState 
       * during the discretization error computation
       */	 
      std::vector< std::pair<int, FlatState>,
        Eigen::aligned_allocator<std::pair<int, FlatState> > 
        > _FlatState_stock;		 	 

      /**
       * Profiles the methods computational times
       */
      Profiler* _profiler;
      
    };

}

#include "solver.cpp"
