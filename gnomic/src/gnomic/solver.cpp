#include <iostream>

namespace gnomic{

  // Readability template definitions
#define TEMPLATE_Loss_Input_State_FlatState template< template <typename,typename,typename> typename Loss,typename Input,typename State,typename FlatState>
#define LOSS_enable typename std::enable_if<std::is_base_of<BaseLoss<Input, State, FlatState>, Loss<Input, State, FlatState>>::value>::type

  using namespace std;  

  TEMPLATE_Loss_Input_State_FlatState
  Solver< Loss<Input, State, FlatState> , LOSS_enable >::Solver() {
    _loss = 0;
    _initial_state = 0;
    _final_state = 0;
    _initial_input = 0;
    _tf = 0.f;
    _N = 0;
    _iterations = 10;
    _perform_all_iterations = false;
    _error_dim = 0;
    _parameters_dim = 0;
    _chi2_prev = 0;
    _hysteresis_size = 3;
    _higher_chi.resize(_hysteresis_size);
    _verbose = false;
    _time_mesh_refinement = false;
    _max_pyramidal_refs = 2;
    _tm_epsilon = 1e-5f;

    std::vector<std::string> profiler_elements;
    profiler_elements.push_back("initializeOptimalTrajectory");
    profiler_elements.push_back("initializeMatrices");
    profiler_elements.push_back("init");
    profiler_elements.push_back("linearize");
    profiler_elements.push_back("updateXi");
    profiler_elements.push_back("discretizationErrorCheck");
    profiler_elements.push_back("timeMeshRefinement");
    profiler_elements.push_back("compute");

    _profiler = new Profiler(profiler_elements);
    
  }


  TEMPLATE_Loss_Input_State_FlatState
  void Solver< Loss<Input, State, FlatState> , LOSS_enable>::initializeOptimalTrajectory(const TrajectoryInitialization& initFlag) {
    _profiler->element("initializeOptimalTrajectory").start();
    //first one is the initial state
    BaseFlatState* base_flat = _initial_state->getFlatState();    
    FlatState* flat_state = (FlatState*)dynamic_cast<FlatState* >(base_flat);
    if(flat_state)
      _optimal_trajectory[0] = *flat_state;
    //_optimal_trajectory[0] = FlatState(_initial_state->getFlatState());
    delete base_flat;
    
    //last one is the final state
    base_flat = _final_state->getFlatState();
    flat_state = dynamic_cast<FlatState* >(base_flat);
    if(flat_state)
      _optimal_trajectory[_N] = *flat_state;
    //_optimal_trajectory[_N] = FlatState(_final_state->getFlatState());
    delete base_flat;
   
    const FlatState diff = _optimal_trajectory[_N] - _optimal_trajectory[0];
    _dt_vec = std::vector<real>(_N, _tf/_N);

    //compute the intermediate state
    for(int iter = 1; iter < _N; ++iter) {
      real step = (real)iter/(_N);
      FlatState product = diff * step;
      
      if(initFlag == TrajectoryInitialization::ZEROS) {
        _optimal_trajectory[iter] = FlatState();     
      }
      else if(initFlag == TrajectoryInitialization::PREVIOUS && _backup_optimal_trajectory.size() == _N + 1 ) {
        _optimal_trajectory[iter] = _backup_optimal_trajectory[iter];
      }
      else { //(initFlag == TrajectoryInitialization::LINEAR) 
        _optimal_trajectory[iter] = _optimal_trajectory[0] + product; 
      }

      if(_verbose){
        std::cerr << FGRN("[Solver][InitTraj]: done") << std::endl;
        _optimal_trajectory[iter].print();
      }
    }   
    _profiler->element("initializeOptimalTrajectory").stop();
  }

  TEMPLATE_Loss_Input_State_FlatState
  void Solver< Loss<Input, State, FlatState> , LOSS_enable>::initialize_matrices(const int error_dim_,
                                                                                 const int params_dim_,
                                                                                 const int N_){
    _profiler->element("initializeMatrices").start();
    _H = MatrixX(params_dim_, params_dim_);
    _b = VectorX(params_dim_);
    _error = VectorX(error_dim_);
    _delta_x = VectorX(params_dim_);
    _J = MatrixX(error_dim_, params_dim_);
    _backup_initials.resize( N_ );
    if(_verbose)
      std::cerr << FGRN("[Solver][InitMat]: done") << std::endl;
    _profiler->element("initializeMatrices").stop();        
  }
  
  
  TEMPLATE_Loss_Input_State_FlatState
  void Solver< Loss<Input, State, FlatState> , LOSS_enable>::init(const TrajectoryInitialization& initFlag){
    _profiler->element("init").start();

    if(!_loss )
      throw std::runtime_error("[Solver]: Missing Loss Initialization!\n");
    if(_N < 2 || _tf == 0.f)
      throw std::runtime_error("[Solver]: Missing Timing Variables Initialization!\n");
    if(!_initial_state || !_final_state)
      throw std::runtime_error("[Solver]: Missing Init/Final State Initialization!\n");   
    if(!_initial_input)
      throw std::runtime_error("[Solver]: Missing Initial Input Initialization!\n");
    
    // resize and init the optimal_trajectory vector
    _optimal_trajectory.resize(_N+1, FlatState());
    initializeOptimalTrajectory(initFlag);
    
    //init optimization matrices
    _error_dim = _total_error_size * (_N-1);
    _parameters_dim = FlatState::size*(_N-1);
    initialize_matrices(_error_dim, _parameters_dim, _N);
    
    _profiler->element("init").stop();
  }

  
  TEMPLATE_Loss_Input_State_FlatState
  void Solver< Loss<Input, State, FlatState> , LOSS_enable>::linearize(const int N_,
                                                                       const std::vector<real>& dt_vec_,
                                                                       const std::vector<FlatState>& optimal_trajectory_,
                                                                       const State& final_state_){
    _profiler->element("linearize").start();

    _H.setZero();
    _b.setZero();   
    _error.setZero();
    
    State statei = *_initial_state;    
    Input inputi = *_initial_input;

    _backup_initials[0] = std::make_pair(statei, inputi);
    ErrorVector error_block;
    
    //Populating Error
    for(int i=1; i < N_; ++i) {
      
      const FlatState& x0 = optimal_trajectory_[i-1];
      const FlatState& xf = optimal_trajectory_[i];
      
      _loss->computeLoss(x0, xf, final_state_, statei, inputi, error_block, dt_vec_[i-1]);
      _error.block<_total_error_size, 1>((i-1)*_total_error_size,0) = error_block;
      _backup_initials[i] = std::make_pair(statei, inputi);     
    }

   
    State statei_minus = State();
    State statei_plus = State();
    Input inputi_plus = Input();    
    Input inputi_minus = Input();    
    
    _J.setZero();
    
    const real epsilon = 1e-4;
    const real i_epsilon = 1/(2*epsilon);
    
    FlatState x0, xf_plus, xf_minus, x0_plus, x0_minus, xf;
    ErrorVector loss_plus, loss_minus;
    
    for(int block_id=0; block_id < (N_-1); ++block_id) {      
      for(int block_col=0; block_col < FlatState::size; ++block_col) {

        statei_plus = _backup_initials[block_id].first;
        statei_minus = _backup_initials[block_id].first;
        inputi_plus = _backup_initials[block_id].second;
        inputi_minus = _backup_initials[block_id].second;
        x0 = _optimal_trajectory[block_id];
        xf_plus = _optimal_trajectory[block_id+1];
        xf_minus = _optimal_trajectory[block_id+1];
	
        xf_plus.ptr()[block_col] += epsilon;
        xf_minus.ptr()[block_col] -= epsilon;
	
        _loss->computeLoss(x0,xf_plus,  final_state_,statei_plus, inputi_plus, loss_plus, dt_vec_[block_id]);
        _loss->computeLoss(x0,xf_minus, final_state_, statei_minus, inputi_minus, loss_minus, dt_vec_[block_id]);
        _J.block< _total_error_size, 1>(block_id*_total_error_size, block_id*FlatState::size+block_col) = i_epsilon*(loss_plus - loss_minus);
	
	  
        /*// TRIANGULAR JACOBIAN
        for(int block_row = block_id + 1; block_row < N_-1; ++block_row)
          {
            x0_plus = xf_plus;
            x0_minus = xf_minus;
            xf = optimal_trajectory_[block_row+1];
            _loss->computeLoss(x0_plus,xf, final_state_, statei_plus, inputi_plus, loss_plus, dt_vec_[block_row]);
            _loss->computeLoss(x0_minus,xf, final_state_, statei_minus, inputi_minus, loss_minus, dt_vec_[block_row]);
            _J.block< _total_error_size, 1>(block_id*_total_error_size+_total_error_size*(block_row-block_id), block_id*FlatState::size+block_col) = i_epsilon*(loss_plus - loss_minus);
          }	
	*/
        // BLOCK DIAGONAL JACOBIAN
        int block_raw = block_id + 1;
        if(block_raw < N_- 1) {
          x0_plus = xf_plus;
          x0_minus = xf_minus;
          xf = _optimal_trajectory[block_raw+1];
          _loss->computeLoss(x0_plus,xf, final_state_, statei_plus, inputi_plus, loss_plus, dt_vec_[block_raw]);
          _loss->computeLoss(x0_minus,xf, final_state_, statei_minus, inputi_minus, loss_minus, dt_vec_[block_raw]);
          _J.block<_total_error_size ,1>(block_id*_total_error_size + _total_error_size*(block_raw-block_id), block_id*FlatState::size+block_col) = i_epsilon*(loss_plus - loss_minus);
          }
      }      
    } 

    
    _H += _J.transpose() * _J;
    _b += _J.transpose() *_error;
    
    _profiler->element("linearize").stop();
  }
  
  TEMPLATE_Loss_Input_State_FlatState
  void Solver< Loss<Input, State, FlatState> , LOSS_enable>::updateXi(std::vector<FlatState>& trajectory_,
                                                                      const gnomic::VectorX& delta_x_,
                                                                      const int parameters_dim_) {
    _profiler->element("updateXi").start();
    int id=1;
    const int trajectory_size = trajectory_.size();
    for(int i=0; i < parameters_dim_; i+=FlatState::size, id++) {
      trajectory_[id].update(delta_x_.block<FlatState::size,1>(i,0));
    }
    _profiler->element("updateXi").stop();
  }


  template<typename Flat>
  void printOptimalTrajectory(std::vector<Flat>& traj) {
    for(int i=0; i < traj.size(); ++i)
      traj[i].print();
  }


  TEMPLATE_Loss_Input_State_FlatState
  void Solver< Loss<Input, State, FlatState>, LOSS_enable>::discretizationErrorCheck(real& discretization_error,
                                                                                     const real& dt,
                                                                                     const State& state,
                                                                                     const State& next_state,
                                                                                     const FlatState& trajectory_flat_state,
                                                                                     const FlatState& next_trajectory_flat_state) {

    
    const real t = 0.9; //compute the interpolation at 0.9 between 0 and 1    
    FlatState penultimate_flat;
    // interpolate
    puntualCubicHermiteInterpolation(penultimate_flat, state, next_state, dt, t);
    State statei, statef;
    Input inputi;
    _loss->_robot->computeState(statei, state, trajectory_flat_state, penultimate_flat, t*dt);
    _loss->_robot->computeInput(statei, inputi);
    _loss->_robot->predictState(statei, inputi, statef, (1.f-t)*dt);
    FlatState error = next_trajectory_flat_state - *(FlatState*)dynamic_cast<FlatState*>(statef.getFlatState());
    for(unsigned int flat_iter = 0; flat_iter < FlatState::size; ++flat_iter)
      discretization_error += abs(error.ptr()[flat_iter]);

  }
  


  TEMPLATE_Loss_Input_State_FlatState
  std::vector<FlatState> Solver< Loss<Input, State, FlatState> , LOSS_enable>::timeMeshRefinement() {
    _profiler->element("timeMeshRefinement").start();
    int tm_N = 2;
    int refinement_iter = 0;
    
          std::vector<std::pair<State, Input>,
                  Eigen::aligned_allocator<std::pair<State, Input> > > tm_state_input_pairs;
    
    // resize the optimal trajectory to the first _N+1 Flat, simple case {0, 1, 2}
    std::vector<FlatState> tm_optimal_trajectory;
    tm_optimal_trajectory.resize(tm_N+1, FlatState());
    for(size_t i = 0; i < tm_N+1; ++i)
      tm_optimal_trajectory[i] = _optimal_trajectory[i];
    
    // resize the corresponding dt
    std::vector<real> tm_dt_vec;
    tm_dt_vec.resize(tm_N);
    for(size_t i = 0; i < tm_N; ++i)
      tm_dt_vec[i] = _dt_vec[i];
    
    if(_verbose) {
      std::cerr << FGRN("[SOLVER][TimeMeshRef]: resizing trajectory:\n");
      printOptimalTrajectory(tm_optimal_trajectory);
      std::cerr << FGRN("[SOLVER][TimeMeshRef]: resizing dt:\n");
      std::cerr << "dt_vec: ";
      for(int i = 0; i < tm_dt_vec.size(); ++i)
        std::cerr << tm_dt_vec[i] << " ";
      std::cerr << std::endl;    
    }
    
    int N_increased = tm_N;
    int shifted_position = 0;
    bool refinement_needed = false;
    // iterate for the required pyramid(s)
    for(size_t pyramid = 0; pyramid < _max_pyramidal_refs; ++pyramid) {
      std::vector<real> discretization_error(N_increased, 0.f);
      std::vector<bool> need_refinement(N_increased, false);
    
      State statei;    
      Input inputi;    
      //0. Computing the pairs <State, Input> along the whole timeMeshRefinement grid
      tm_state_input_pairs.push_back( std::make_pair(*_initial_state, *_initial_input) );
      for(size_t compute_iter = 0; compute_iter < tm_optimal_trajectory.size()-1; ++compute_iter){
        _loss->_robot->computeState(statei, tm_state_input_pairs[compute_iter].first, tm_optimal_trajectory[compute_iter], tm_optimal_trajectory[compute_iter+1], tm_dt_vec.at(compute_iter));
        _loss->_robot->computeInput(statei, inputi);
        tm_state_input_pairs.push_back( std::make_pair(statei, inputi) ); 
      }

      //1. Check Need of Refinement
      for(size_t snap = 0; snap < discretization_error.size(); ++snap){
        need_refinement[snap] = true;
        discretizationErrorCheck(discretization_error[snap],
                                 tm_dt_vec[snap],
                                 tm_state_input_pairs[snap].first,
                                 tm_state_input_pairs[snap+1].first,
                                 tm_optimal_trajectory[snap],
                                 tm_optimal_trajectory[snap+1]);
        if(discretization_error[snap] > _tm_epsilon)
          need_refinement[snap] = true;
        refinement_needed |= need_refinement[snap];
      }

      
      //2. Where needed, generate interpolated flat at half the time (t = .5f)
      const real t_interpolated_flat = .5f;
      std::vector<FlatState> interpolated_flat(tm_N, FlatState());
      for(size_t snap = 0; snap < tm_N; ++snap) {
        const real& dt = tm_dt_vec[snap];
        FlatState& half_flat = interpolated_flat[snap];
        puntualCubicHermiteInterpolation(half_flat,
                                         tm_state_input_pairs[snap].first,
                                         tm_state_input_pairs[snap+1].first,
                                         dt,
                                         t_interpolated_flat);      
      }
      

      if(_verbose) {
        std::cerr << FYEL("Traj:\n");
        printOptimalTrajectory(tm_optimal_trajectory);
        std::cerr << FYEL("Dt: ");
        for(size_t dt_i = 0; dt_i < tm_dt_vec.size(); ++dt_i)
          std::cerr << tm_dt_vec[dt_i] << " ";
        std::cerr << std::endl;
      }
      
      //3. Insert them in the dt and trajectory
      shifted_position = 0;
      for(size_t snap = 0; snap < tm_N; ++snap){
        if(need_refinement[snap]){
          // insert dt
          tm_dt_vec[snap+shifted_position] *= t_interpolated_flat;
          tm_dt_vec.insert(tm_dt_vec.begin()+1+snap+shifted_position,
                           tm_dt_vec[snap+shifted_position]);
          // insert flat
          tm_optimal_trajectory.insert(tm_optimal_trajectory.begin()+1+snap+shifted_position,
                                       interpolated_flat[snap]);
          
          ++shifted_position;
          N_increased++;
        }
      }

      if(_verbose) {
        std::cerr << FYEL("After Traj:\n");
        printOptimalTrajectory(tm_optimal_trajectory);
        std::cerr << FYEL("After Dt: ");
        for(size_t dt_i = 0; dt_i < tm_dt_vec.size(); ++dt_i)
          std::cerr << tm_dt_vec[dt_i] << " ";
        std::cerr << std::endl;
      }

      
    } // end for(pyramid)

    if(!refinement_needed){
      if(_verbose)
        std::cerr << FGRN("[SOLVER][TimeMeshRef]: no need of refinement") << std::endl;
      return _optimal_trajectory;
    }
    
    //4. resize LS problem and set final_state
    const int tm_error_dim = _total_error_size * (N_increased-1);
    const int tm_parameters_dim = FlatState::size*(N_increased-1);
    initialize_matrices(tm_error_dim, tm_parameters_dim, N_increased);    
    
    
    
    std::cout << "stato finale tm: " << tm_state_input_pairs[tm_state_input_pairs.size()-1].first.p.transpose() << std::endl;
        
    std::cout << "dts prima della ottimizzazione: " << tm_dt_vec[0] << " " << tm_dt_vec[1] << " " << 
    tm_dt_vec[2] << " " << tm_dt_vec[3] << " " << std::endl;
    
    printOptimalTrajectory(tm_optimal_trajectory);
    
    //5. solve
    for(size_t iter = 0; iter < 5; ++iter){        
      linearize(N_increased, tm_dt_vec, tm_optimal_trajectory, *_final_state); //tm_state_input_pairs[tm_state_input_pairs.size()-1].first );//*_final_state); 
      real chi2 = _error.squaredNorm();
      const VectorX tm_delta_x = _H.llt().solve(-_b);	
      updateXi(tm_optimal_trajectory, tm_delta_x, tm_parameters_dim);
      if(_verbose)
        std::cout << iter << " " << chi2 << std::endl;
      
      //std::cout << _J << std::endl;
      
      if(isnan(chi2))
	std::exit(1);
      
    }
    
    if(_verbose){
      std::cerr << FRED("Final Small Trajectory") << std::endl;
      printOptimalTrajectory(tm_optimal_trajectory);
    }
    
    //6. merge
    tm_optimal_trajectory.insert(tm_optimal_trajectory.end(),
                                 _optimal_trajectory.begin()+tm_N+1,
                                 _optimal_trajectory.end());
    
    if(_verbose){
      std::cerr << FRED("Merged Trajectory") << std::endl;    
      printOptimalTrajectory(tm_optimal_trajectory);
    }
     
    tm_dt_vec.insert(tm_dt_vec.end(),
                     _dt_vec.begin()+tm_N,
                     _dt_vec.end());    
    
    _dt_vec = tm_dt_vec;
 
    //7. restore with old sizes
    const int final_error_dim = _total_error_size *(_N+2-1);//(N_increased+(_N-tm_N)-1);
    const int final_parameters_dim = FlatState::size*(_N+2-1);//(N_increased+(_N-tm_N)-1);
    initialize_matrices(final_error_dim, final_parameters_dim, _N+2);//N_increased+(_N-tm_N)-1);
    
//     std::cout << tm_optimal_trajectory.size() << " " << _dt_vec.size() << std::endl;
// 
//     for(unsigned int iter = 0; iter < 5; iter++)
//       {
// 	linearize(_N+2, _dt_vec, tm_optimal_trajectory, *_final_state);
// 	real chi2_ = _error.transpose() * _error;
// 	// solve the linearized system
// 	_delta_x = _H.llt().solve(-_b);
// 	// update the solution
// 	updateXi(tm_optimal_trajectory, _delta_x, final_parameters_dim);
// 	std::cout << chi2_ << std::endl;
//       }
      
    _profiler->element("timeMeshRefinement").stop();
    
    //8. return merged trajectory
    return tm_optimal_trajectory;

  }

  
  
  TEMPLATE_Loss_Input_State_FlatState
  void Solver< Loss<Input, State, FlatState> , LOSS_enable>::compute() {
    _profiler->element("compute").start();
    
    std::fill(_higher_chi.begin(), _higher_chi.end(), false);
    std::vector<bool>::iterator hysteresis_it = _higher_chi.begin();
    _best_optimal_trajectory = _optimal_trajectory;
    real best_chi = FLT_MAX;
    real best_chi_hist = FLT_MAX;
    bool hist_termination = false;

    auto compute_start = std::chrono::high_resolution_clock::now();

    
    for(int i=0; i < _iterations; ++i){

      // linearize by computing Hessian and Residual
      linearize(_N, _dt_vec, _optimal_trajectory, *_final_state);
      
      real chi2 = _error.transpose() * _error;

      // solve the linearized system
      _delta_x = _H.llt().solve(-_b);

      // update the solution
      updateXi(_optimal_trajectory, _delta_x, _parameters_dim);

      if(_verbose)
        std::cerr << FGRN("[SOLVER][compute]") << " it: " << i << " chi2: " << chi2 << " chi2_diff: " << _chi2_prev - chi2 << std::endl;

      if(!_perform_all_iterations) {
        if(chi2 < best_chi){
          best_chi = chi2;
          _best_optimal_trajectory = _optimal_trajectory;
        }
      
        if(chi2 < ( best_chi_hist - .05*best_chi_hist) ) {
          best_chi_hist = chi2;
          std::fill(_higher_chi.begin(), _higher_chi.end(), false);
        } else {
          *hysteresis_it = true;
          bool stop = true;
          for(size_t i = 0; i < _hysteresis_size; ++i)
            stop &= _higher_chi[i];
          if(stop){
            hist_termination = true; 
            break;
          }
          if (hysteresis_it == _higher_chi.end())
            hysteresis_it = _higher_chi.begin();
          else
            ++hysteresis_it;
        }
        
      } // if(!_perform_all_iterations)     
     
      _chi2_prev = chi2;
      
    } //end for(iterations)

    if(!_perform_all_iterations)
      _optimal_trajectory = _best_optimal_trajectory;

    if(_verbose) {
      std::cerr << FGRN("[SOLVER][compute]: solution found") << std::endl;
      printOptimalTrajectory(_optimal_trajectory);
    }
    
    // If required, call the refinement of the solution
    if(_time_mesh_refinement) {
      if(_verbose)
        std::cerr << FGRN("[SOLVER][compute]: call to refinement") << std::endl;
      auto mesh_refinement_start = std::chrono::high_resolution_clock::now();
      _final_optimal_trajectory = timeMeshRefinement();
      


      
      if(_verbose)
        std::cerr << FGRN("[SOLVER][compute]: back from refinement") << std::endl;
      auto mesh_refinement_end = std::chrono::high_resolution_clock::now();      
      if(_verbose) {
        double mesh_refinement_time = std::chrono::duration_cast<std::chrono::milliseconds>(mesh_refinement_end - mesh_refinement_start).count();
        std::cout << FYEL("[SOLVER][compute]: Time Elapsed for refinement: ") << mesh_refinement_time << " milliseconds" << std::endl;
        std::cerr << FGRN("[SOLVER][compute]: solution found after refinement") << std::endl;
        printOptimalTrajectory(_final_optimal_trajectory);
      }      
    }else{
      _final_optimal_trajectory = _optimal_trajectory;
    }
    
    auto compute_end = std::chrono::high_resolution_clock::now();

    if(_verbose) {
      std::string termination_criteria = hist_termination ? FGRN("by histeresis termination criteria") : FGRN("because the max. number of iterations has been reached");
      std::cerr << FGRN("[SOLVER][compute]: Optimization ended ") << termination_criteria << std::endl;
      std::cerr << FGRN("[SOLVER][compute]: Returned solution with chi2: ") << best_chi << std::endl;
      double compute_time = std::chrono::duration_cast<std::chrono::milliseconds>(compute_end- compute_start).count();
      std::cout << FYEL("[SOLVER][compute]: Time Elapsed for finding a solution: ") << compute_time << " milliseconds" << std::endl;
    }

    _profiler->element("compute").stop();        
  }
  

  TEMPLATE_Loss_Input_State_FlatState
  State Solver< Loss<Input, State, FlatState> , LOSS_enable>::optimalTrajectory(const real& t_) {
    if(t_ >= _tf)
      throw std::runtime_error("[SOLVER][optimalTrajectory]: required a Flat at t >= tf!");

    State s0, s1, solution_state;
    s0 = *_initial_state;
    real curr_t = 0.f;
    FlatState solution_flat;
    Input solution_input;

    // compute required Flat and State interpolating from solution
    const int solution_size = _optimal_trajectory.size();
    for(int i = 1; i < solution_size; ++i) {
      curr_t += _dt_vec[i-1];     
      _loss->_robot->computeState(s1,
                                  s0,
                                  _optimal_trajectory[i-1],
                                  _optimal_trajectory[i],
                                  _dt_vec[i-1]);
      if(fabs(curr_t - t_) < 1e-4) {
        //        std::cerr << "PUTTANA MAIALA DYBALAAAAA" << std::endl;
        return s1;
      }
      if(curr_t >= t_){
        const real t = (t_ - (curr_t - _dt_vec[i-1]))/_dt_vec[i-1];
        //std::cerr << t_ << " " << t << " " << curr_t << " " << _dt_vec[i-1] << std::endl;  
        puntualCubicHermiteInterpolation(solution_flat, s0, s1, _dt_vec[i-1], t);
        _loss->_robot->computeState(solution_state,
                                    s0,
                                    _optimal_trajectory[i-1],
                                    solution_flat,
                                    t_ - (curr_t - _dt_vec[i-1]));
        
        return solution_state;
      }
      s0 = s1;        
    }

    // // compute corresponding Input
    // _loss->_robot->computeInput(solution_state, solution_input);
    // return solution_input;
    
  }
  

  
}

