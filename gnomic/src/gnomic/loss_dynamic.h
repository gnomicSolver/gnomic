#pragma once
#include <gnomic/base_loss.h>

namespace gnomic {

  /**
   * A Loss function implementation. It inheriths from BaseLoss,
   * and is implicitly forced by the BaseLoss limitations to use types
   * derived from gnomic::BaseTypes (BaseInputVector, BaseStateVector, BaseFlatState)
   */
  template<typename Input,typename State,typename FlatState>
    struct Loss: public BaseLoss<Input, State, FlatState> {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
      
    /** 
     * This static variable stores the LossVector size, i.e. the error size
     * it is used by the solver at compile-time to resize the
     * Error-related matrices/Vectors.
     * In this case the size is defined considering the cost function:
     *         Sf*(x-xf)  +    R(u)    +   Al*(x(k)-x(k-1))
     * i.e.   state_size  + input_size +      state_size
     */
    const static int size = State::size + Input::size + State::size + 1;

    /** 
     * typedefs
     */
    typedef Eigen::Matrix<float, State::size, 1> StateErrorVector;
    typedef Eigen::Matrix<float, Input::size, 1> InputErrorVector;

    /**
     * Weights used in the context of the Loss computation
     * they are visible to the user that can modify them
     * accordingly to the cost function
     */
    struct Weights{
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

      /** 
       * typedefs
       */
      typedef Eigen::Matrix<float, Input::size, Input::size> InputWeightMatrix;
      typedef Eigen::Matrix<float, State::size, State::size> StateWeightMatrix;
		
      /** 
       * weights the input
       */
      InputWeightMatrix R;
	
      /**
       * weights the difference between current state
       * and final state
       */
      StateWeightMatrix Sf;

      /**
       * weights the continuity
       * constraints, also known as
       * Albrdo matrix
       */
      StateWeightMatrix Al;
	
      /** 
       * Constructor. 
       * Set to Identity all the matrices
       */
      Weights(){
        R = R.setIdentity();
        Sf = Sf.setIdentity();
        Al = Al.setIdentity();
      }

      /**
       * sets the coefficients to a specified Weights value.
       * Prints out the current weights.
       * @param weights weights structure
       */
      void SetCoeffs(const Weights& weights){
        R = weights.R.array().sqrt();
        Sf = weights.Sf.array().sqrt();
        Al = weights.Al.array().sqrt();
	
        std::cout << '\n';
        std::cout << " Sf Weight Matrix: " << '\n';
        std::cout << Sf << '\n' << '\n';
        std::cout << " Al Weight Matrix: " << '\n';
        std::cout << Al << '\n' << '\n';
        std::cout << " R Weight Matrix: " << '\n';
        std::cout << R << '\n' << '\n';
      }
    };

    /**
     * computes the loss function.
     * This loss function is computed as:
     * Sf*(x-xf)  +    R(u)    +   Al*(x(k)-x(k-1))
     * @param xi0
     * @param xif
     * @param final_state
     * @param statei
     * @param inputi
     * @param outputi
     * @param dt
     */
    template<typename ErrorSize>
      inline void computeLoss(const FlatState& xi0,
                              const FlatState& xif,
                              const State& final_state,
                              State& statei,
                              Input& inputi,
                              Eigen::MatrixBase<ErrorSize>& output,
                              const float& dt) {
      _robot->computeState(_loss_state, statei, xi0, xif, xif.dt);
      StateErrorVector state_diff = (_loss_state - final_state);  
      //* first error component: S*(x - xf)
      output.block(0, 0, State::size,1) = _weights.Sf*state_diff;
      _robot->predictState(statei, inputi, _loss_pred_state, xif.dt); 
      //* keep in mind that inputi is overwritten here
      _robot->computeInput(_loss_state, inputi);
      //* second error component: R*(u)
      output.block( State::size, 0, Input::size,1) = _weights.R*( inputi.getVector().cwiseProduct(inputi.getVector()) );     
      StateErrorVector dynamic_diff = (_loss_state-_loss_pred_state);     
      //* third error component: Albrdo*(x(k)-x(k-1))
      output.block( State::size+Input::size, 0, State::size,1) = _weights.Al*dynamic_diff;
      //* last element, the dt should be close to the reference dt
      output(State::size+Input::size+State::size) = dt-xif.dt;
      
      statei = _loss_state;
      _loss_state.reset();
      _loss_pred_state.reset();	
    };

    /**
     * sets the robotDynamics needed to compute the loss function
     */
    inline void setRobotDynamics(BaseRobotDynamics<Input, State, FlatState>* robot){
      _robot = robot;
    }

    /**
     * sets the weights
     */
    inline void setWeights(const Weights& weights){
      _weights.R = weights.R.array().sqrt();
      _weights.Sf = weights.Sf.array().sqrt();
      _weights.Al = weights.Al.array().sqrt();

      std::cout << '\n';
      std::cout << " Sf Weight Matrix: " << '\n';
      std::cout << _weights.Sf << '\n' << '\n';
      std::cout << " Al Weight Matrix: " << '\n';
      std::cout << _weights.Al << '\n' << '\n';
      std::cout << " R Weight Matrix: " << '\n';
      std::cout << _weights.R << '\n' << '\n';
    }

    /**
     * current loss state
     */
    State _loss_state;

    /**
     * predicted loss state
     */
    State _loss_pred_state;

    /**
     * pointer to robotDynamics
     */
    BaseRobotDynamics<Input, State, FlatState>* _robot;

    /**
     * the weights
     */
    Weights _weights;
      
  };

  
}
