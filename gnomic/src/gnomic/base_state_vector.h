#pragma once
#include "utils.hpp"
#include "base_flat_state.h"

namespace gnomic{

  /**
   * Base class for a StateVector, it has to be inherited.
   * When inheriting:
   *      - the static size has to be re-defined
   *      - the operator- has to be re-defined
   *      - the reset method has to be overridden
   *      - the getFlatState method has to be overridden
   *      - the print method has to be overridden
   */
  struct BaseStateVector{

    /** 
     * This static variable stores the BaseStateVector size.
     * It is used in the Solver to resize matrices at compile time.
     * It has to be re-defined in the derived struct(s)
     */
    const static int size = -1;
    const static int constraints_size = 0;
    
    /**
     * Allows the minus operation between BaseStateVector (s).
     * It has to be overridden by inherited class.
     * @param a the right value of the operator-
     */
    BaseStateVector &operator-(const BaseStateVector& a) const {
      throw std::runtime_error("[GNOMIC][BaseStateVector]: you should override operators in your derived StateVector!");
    }    

    /**
     * Resets the members of a BaseStateVector.
     * It has to be overridden by inherited class.
     */
    virtual void reset() = 0;    

    /**
     * Returns a pointer to the respective BaseFlatState.
     * It has to be overridden by inherited class
     * @return pointer to the respective BaseFlatState.
     */
    virtual BaseFlatState* getFlatState( ) const  = 0;    

    /**
     * Returns a pointer to the respective BaseFlatState.
     * It has to be overridden by inherited class
     * @return pointer to the respective BaseFlatState.
     */
    virtual BaseFlatState* getDeltaFlatState( ) const = 0;

    
    /**
     * Prints in terminal the content of the BaseFlatState
     * It has to be overridden by inherited class    
     */
    virtual void print() const = 0;
    
  };
  
}
