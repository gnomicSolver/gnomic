#pragma once
#include "utils.hpp"
#include "base_flat_state.h"
#include "base_input_vector.h"
#include "base_state_vector.h"
#include "base_robot_dynamics.h"

namespace gnomic {

  template<typename Input,typename State,typename FlatState, typename Enable = void>
    struct BaseLoss;    
  /**
   * Base class for a Loss, it has to be inherited.
   * It is a template class <Input, State, FlatState>.
   * When inheriting:
   *      - the static size has to be re-defined
   *      - the computeLoss method has to be overridden
   *      - the user is forced to use types inherited from our Base type, i.e.
   *          - Input has to be of type BaseInputVector
   *          - State has to be of type BaseStateVector
   *          - FlatState has to be of type BaseFlatState
   * Example:
   *
   * MyLoss<Input, State, FlatState> : public BaseLoss<Input, State, FlatState>
   * can be instantiated iff:
   *      - struct Input : public BaseInputVector
   *      - struct State : public BaseStateVector
   *      - struct FlatState : public BaseFlatState
   */
  template<typename Input, typename State, typename FlatState>
    struct BaseLoss<Input, 
    State, 
    FlatState,
    typename std::enable_if<std::__and_<std::is_base_of<BaseFlatState, FlatState>, std::is_base_of<BaseInputVector, Input> , std::is_base_of<BaseStateVector, State>>::value>::type> {

    /**
     * This static variable stores the LossVector size, i.e. the error size
     * it is used by the solver at compile-time to resize the
     * Error-related matrices/Vectors.
     * It has always to be re-defined when inherit this struct!
     */
    const static int size = -1;

    /**
     * Computes the loss.
     * It has to be overridden by inherited class.
     * @param xi0
     * @param xif
     * @param final_state
     * @param statei
     * @param inputi
     * @param outputi
     * @param dt
     */
    template<typename ErrorSize>
      void computeLoss(const FlatState& xi0,
                       const FlatState& xif,
                       const State& final_state,
                       State& statei,
                       Input& inputi,
                       Eigen::MatrixBase<ErrorSize>& outputi,
                       const real& dt){
      throw std::runtime_error("[Gnomic][BaseLoss]: you forgot to define the computeLoss!");
    };

  };
  
}

