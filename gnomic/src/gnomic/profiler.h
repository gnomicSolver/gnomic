#pragma once
#include <chrono>
#include <float.h>
#include <iostream>
#include <map>
#include "colors.h"

namespace gnomic{
  struct Element {
    Element(){
      min = DBL_MAX;
      max = 0.0;
      tot = 0.0;
      calls = 0;
    }
    
    void start(){
      calls++;
      start_time = std::chrono::high_resolution_clock::now();
    }
    void stop() {
      time_span = std::chrono::high_resolution_clock::now() - start_time;
      double step_time = time_span.count();
      tot += step_time;
      min = std::min(min, step_time);
      max = std::max(max, step_time);
    }

    friend std::ostream& operator<<(std::ostream& os, Element const & el) {
      return os << " tot:   " << el.tot << std::endl
                << " mean:  " << el.tot/(double)el.calls << std::endl
                << " min:   " << el.min << std::endl
                << " max:   " << el.max << std::endl
                << " calls: " << el.calls;
    }
    
    double min;
    double max;
    double tot;
    int calls;
    std::chrono::high_resolution_clock::time_point start_time;
    std::chrono::duration<double> time_span;
  };


  class Profiler {
    typedef std::map<std::string, Element> StringElementMap;
  public:
    Profiler(const std::vector<std::string>& profiled_elements_) {
      for(size_t i = 0; i < profiled_elements_.size(); ++i) {
        Element ith_element;
        _elements.insert(std::pair<std::string, Element>(profiled_elements_[i], ith_element));
      }
    }
    
    Element& element(const std::string& query){
      return _elements.at(query);
    }

    const StringElementMap& elements() const {return _elements;}
    
    friend std::ostream& operator<<(std::ostream& os, Profiler const& p) {
      const StringElementMap& el = p.elements();
      os << FBLU("[PROFILER]") << std::endl;
      for(StringElementMap::const_iterator it = el.begin();
          it != el.end(); ++it) {
        os << KBLU << "element: "
           << it->first << RST << std::endl
           << it->second << std::endl;
      }
      return os;
    }
        
  private:
    Profiler(){}
    StringElementMap _elements;
  };



}
