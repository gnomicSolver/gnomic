#pragma once
#include <Eigen/Core>
#include <Eigen/Geometry>
#include "defs.h"
#include "utils.hpp"
#include <iostream>

namespace gnomic {

  /**
   * Base class for a RobotDynamics.
   * It is a template class <Input, State, FlatState>.
   * When inheriting:
   *      - the computeInput method has to be overridden
   *      - the computeState method has to be overridden
   *      - the predictState method has to be overridden
   */
  template<typename Input, typename State, typename FlatState>
    class BaseRobotDynamics {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    /**
     * Base constructor passing the mass of the robot, stored in a 
     * float member, and the inertia, as a 3x3 float matrix.
     * @param mass float expressing the robot mass
     * @param inertia 3x3 matrix expressing the inertia
     */
    BaseRobotDynamics(const real& mass, const Matrix3& inertia){
      _m = mass;
      _inertia = inertia;
    }

    /**
     * Destructor
     */
    ~BaseRobotDynamics(){
      std::cerr << "[BaseRobotDynamics] deleted\n";
    }

    /** 
     * Computes the Input.
     * This method has to be overridden.
     * @param state
     * @param input
     */
    virtual void computeInput(const State& state,
                              Input& input) = 0;

    /**
     * Computes the State.
     * @param state
     * @param state0
     * @param xi0
     * @param xif
     * @param dt
     */
    virtual void computeState(State& state,
                              const State& state0,
                              const FlatState& xi0,
                              const FlatState& xif, const real& dt) = 0;

    /**
     * Predicts the State.
     * @param statei0
     * @param input
     * @param stateif
     * @param dt
     */
    virtual void predictState(const State& statei0, 
                              const Input& input, 
                              State& stateif, const real& dt) = 0;		      
    
    /**
    * Compute Additional Constraints.
    * @param statei0
    * @param input
    */
    virtual void computeConstraints(const FlatState& x0,
				    const FlatState& xf,
				    MatrixX& constraint,
				    const real& dt) = 0;	    
			      
  protected:
    /** 
     * The Robot mass
     */
    real _m;

    /** 
     * The Robot inertia matrix
     */
    Matrix3 _inertia; 
    Matrix3 _dragMatrix;
  };

}
