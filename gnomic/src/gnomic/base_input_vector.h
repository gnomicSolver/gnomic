#pragma once
#include "utils.hpp"

namespace gnomic {

  /**
   * Base class for a InputVector, it has to be inherited.
   * When inheriting:
   *      - the static size has to be re-defined
   *      - the getVector method has to be overridden
   *      - the print method has to be overridden
   */
  struct BaseInputVector {
    
    /**
     * This static variable stores the InputVector size,
     * it is used by the solver at compile-time to resize the
     * Input-related matrices.
     * It has always to be re-defined when inherit this struct!
     */
    const static int size = -1;
    
    /**
     * Returns the content of an InputVector as an Eigen::Vector
     * It has to be overridden by inherited class     
     * @return the input expressed as an Eigen::Vector
     */
    template<typename Derived>
    Eigen::MatrixBase<Derived> getVector(){
      throw std::runtime_error("[GNOMIC][BaseInputVector]: you should override getVector() method in your derived InputVector!");
    }

    /**
     * Prints in terminal the content of the BaseInputVector
     * It has to be overridden by inherited class 
     */
    virtual void print() = 0;
  };
  
}
