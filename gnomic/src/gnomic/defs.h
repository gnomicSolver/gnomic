#pragma once
#include <Eigen/Core>
#include <Eigen/Geometry>

namespace gnomic {

  typedef float real;

  typedef Eigen::Matrix<real, 2, 1> Vector2;
  typedef Eigen::Matrix<real, 3, 1> Vector3;
  typedef Eigen::Matrix<real, 4, 1> Vector4;
  typedef Eigen::Matrix<real, Eigen::Dynamic, 1> VectorX;

  typedef Eigen::Matrix<real, 2, 2> Matrix2;    
  typedef Eigen::Matrix<real, 3, 3> Matrix3;
  typedef Eigen::Matrix<real, Eigen::Dynamic, Eigen::Dynamic> MatrixX;  

  typedef Eigen::Rotation2D<real> Rotation2D;
  typedef Eigen::Quaternion<real> Quaternion;
  typedef Eigen::AngleAxis<real> AngleAxis;
  
  /**
   * used in the Solver to initialize the trajectory
   */
  enum TrajectoryInitialization{
    ZEROS    = 0,   /**< initialize the trajectory with Zeros */
    LINEAR   = 1,   /**< initialize the trajectory as Linear */
    PREVIOUS = 2    /**< initialize the trajectory from the previous solution */
  };

  /**
   * gravity acceleration
   */
  const real gravity = -9.80665;

  /**
   * gravity acceleration vector
   */
  const Vector3 gravity_vector(0,0,gravity);
  
}
