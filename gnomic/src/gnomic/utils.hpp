#pragma once
#include <Eigen/Geometry>
#include "defs.h"
#include "colors.h"
#include <iostream>

namespace gnomic {

  template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
  }
  
  gnomic::real normalizeAngle(const gnomic::real& angle){
    return atan2(sin(angle), cos(angle));
    //return remainder( angle, 2.0*M_PI);
  }
  
  
  /**
   * Returns the yaw angle in radians, given an Eigen::Quaternion
   * as input.
   * @param q an Eigen Quaternion
   * @return the corresponding yaw angle [rad]
   */
  template<typename Real>
  inline Real yawFromQuaternion(const Eigen::Quaternion<Real>& q) {
    return atan2(2.0 * (q.w() * q.z() + q.x() * q.y()),
                 1.0 - 2.0 * (q.y() * q.y() + q.z() * q.z()));
  }

  /**
   * Returns the roll angle in radians, given an Eigen::Quaternion
   * as input.
   * @param q an Eigen Quaternion
   * @return the corresponding roll angle [rad]
   */
  template<typename Real>
  inline Real rollFromQuaternion(const Eigen::Quaternion<Real>& q) {
    return atan2(2.0*(q.x()*q.w() + q.y()*q.z()),
                 1 - 2*(q.x()*q.x() + q.y()*q.y()));
  }  

  /**
   * Returns the pitch angle in radians, given an Eigen::Quaternion
   * as input.
   * @param q an Eigen Quaternion
   * @return the corresponding pitch angle [rad]
   */
  template<typename Real>
  inline Real pitchFromQuaternion(const Eigen::Quaternion<Real>& q) {
    return asin(2.0*(q.w()*q.y() - q.x()*q.z()));
  }

  template<typename Real>
  inline Eigen::Matrix<real,3,1> vectorFromSkewMatrix(const gnomic::Matrix3& skew_matrix) {
    Eigen::Matrix<real,3,1> output;
    output << skew_matrix(2, 1), skew_matrix(0,2), skew_matrix(1, 0);
    return output;
  }
  
  /**
   * Returns a Skew-Symmetric matrix built upon the input 3-dimensional vector as:
   *          [ 0 -z  y ]
   *   Skew = [ z  0 -x ]
   *          [-y  x  0 ]
   *
   * @param p 3d vector expressing a 3d coordinate
   * @return a 3x3 matrix built as its skew-symmetric
   */
  template<typename Real>
  inline Eigen::Matrix<Real, 3, 3> skew(const Eigen::Matrix<Real, 3, 1>& p){
    Eigen::Matrix<Real, 3, 3> s;
    s << 
      0,  -p.z(), p.y(),
      p.z(), 0,  -p.x(), 
      -p.y(), p.x(), 0;
    return s;
  }

  /**
   * Cubic Hermite Interpolation: a cubic interpolation that preserves the 
   * differentiability of the interpolated variables (e.g., FlatState)
   * @param output interpolated FlatState
   * @param state0 State used as fixed initial point for the interpolation
   * @param state1 State used as fixed final point for the interpolation
   * @param dt time window between state0 and state1
   * @param t time variables for selecting where to take the interpolated 
   * 	    FlatState in the time windows (value between 0 and 1)
   */
  template<typename State, typename FlatState>
  void puntualCubicHermiteInterpolation(FlatState& interpolated_flat,
                                        const State& state0,
                                        const State& state1,
                                        const real& dt,
                                        const real& t) {
    const real t2 = t*t;
    const real t3 = t2*t;
    
    // p(t) = h00*p0 + h10*dt*m0 + h01*p1 + h11*dt*m1
    const real h00 = 2*t3 - 3*t2 + 1;    
    const real h10 = t3 - 2*t2 + t;
    const real h01 = -2*t3 + 3*t2;    
    const real h11 = t3 - t2;

    FlatState* state_flat_0 = dynamic_cast<FlatState*>(state0.getFlatState());
    FlatState* state_dflat_0 = dynamic_cast<FlatState*>(state0.getDeltaFlatState());
    FlatState* state_flat_1 = dynamic_cast<FlatState*>(state1.getFlatState());
    FlatState* state_dflat_1 = dynamic_cast<FlatState*>(state1.getDeltaFlatState());

    // put same sign to angles (unNormalizing it)
    // to allow a correct interpolation
    state_flat_1->unNormalize(*state_flat_0);
    
    interpolated_flat = *state_flat_0 * h00
      + *state_flat_1 * h01
      + *state_dflat_0 * h10 * dt
      + *state_dflat_1 * h11 * dt;    

    
    // std::cerr << FRED("state0:       ");
    // state_flat_0->print();
    // std::cerr << FRED("state1:       ");
    // state_flat_1->print();
    // std::cerr << FRED("dstate0:      ");
    // state_dflat_0->print();
    // std::cerr << FRED("dstate1:      ");
    // state_dflat_1->print();
    // std::cerr << KYEL
    //           << "h00: " << h00
    //           << " h10: " << h10
    //           << " h01: " << h01
    //           << " h11: " << h11
    //           << " dt:  " << dt << std::endl;
    // std::cerr << KGRN << "interpolated: ";
    // interpolated_flat.print();
    // std::cerr << RST;
    
    delete state_flat_0;
    delete state_flat_1;
    delete state_dflat_0;
    delete state_dflat_1;
    
  }


  
}
