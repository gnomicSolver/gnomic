#pragma once
#include <gnomic/base_robot_dynamics.h>
#include <cmath>

namespace mav_model{
  
  template<typename Input, typename State, typename FlatState>
    class MavDynamics : public gnomic::BaseRobotDynamics<Input, State, FlatState>{
    
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    MavDynamics(const gnomic::real& mass, const gnomic::Matrix3& inertia);
    
    ~MavDynamics(){
      std::cerr << "[MavDynamics]: deleted\n";
    };
       
    
    virtual void computeInput( const State& state,
                               Input& input) {    
      input.thrust = (state.thrust+gnomic::gravity)/(-gnomic::gravity_vector(2)); 
      Eigen::Vector3f tau( this->_inertia*state.dot_omega - (this->_inertia*state.omega).cross( state.omega ) );
      input.tau1 = gnomic::rollFromQuaternion(state.q); //tau(0);  
      input.tau2 = gnomic::pitchFromQuaternion(state.q); //tau(1);  
      input.tau3 = state.dot_psi;//tau(2);
    } 
    
    virtual void computeState( State& state,
                               const State& state0,
                               const FlatState& xi0,
                               const FlatState& xif, const gnomic::real& dt);
    
    virtual void predictState( const State& statei0, 
                               const Input& input, 
                               State& stateif, const gnomic::real& dt );
    
    virtual void computeConstraints(const FlatState& x0,
                                    const FlatState& xf,
                                    gnomic::MatrixX& constraint,
                                    const gnomic::real& dt){};
  private:
  };
  
}

#include <mav_model/mav_dynamics.cpp>
