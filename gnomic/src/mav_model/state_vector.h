#pragma once
#include <gnomic/utils.hpp>
#include <gnomic/base_state_vector.h>
#include "xi.h"

namespace mav_model{
  
  struct StateVector : public gnomic::BaseStateVector{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    
    gnomic::Vector3 p; //position
    gnomic::Quaternion q; //orientation
    gnomic::Vector3 dot_p; //linear velocity
    gnomic::Vector3 omega; //angular velocity
    gnomic::Vector3 dot_omega; //angular acceleration
    gnomic::Vector3 ddot_p; //linear acceleration
    gnomic::Vector3 dddot_p; //jerk
    gnomic::Vector3 ddddot_p; //snap
    gnomic::real dot_psi;
    gnomic::real thrust;
    gnomic::real d_thrust;

    // This static variable stores the stateVector size
    // it has always to be defined!
    const static int size = 10;
    const static int constraints_size = 0;
    typedef Eigen::Matrix<gnomic::real, size, 1> StateErrorVector;
    
    ~StateVector() {
      //std::cerr << "[StateVector]: deleted\n";
    }
    
    StateVector() {
      p.setZero();
      q.setIdentity();
      dot_p.setZero();
      omega.setZero();
      ddot_p.setZero();
      dddot_p.setZero();
      ddddot_p.setZero();
      dot_omega.setZero();
      dot_psi = 0.f;
      thrust = 0.f;    
      d_thrust = 0.f;
    }
    
    StateVector( const StateVector& state) {
      p = state.p;
      q = state.q;
      dot_p = state.dot_p;
      omega = state.omega;
      ddot_p = state.ddot_p;
      dddot_p = state.dddot_p;
      dot_psi = state.dot_psi;
      thrust = state.thrust;
      d_thrust = state.d_thrust;
      ddddot_p = state.ddddot_p;
      dot_omega = state.dot_omega;
    }
    
    StateErrorVector operator-(const StateVector& a) const {
      StateErrorVector vec;
      vec.setZero();
      vec.block<3,1>(0,0) = p - a.p;
      gnomic::Matrix3 R_des = a.q.toRotationMatrix();
      gnomic::Matrix3 R = q.toRotationMatrix();
      vec.block<3,1>(3,0) = gnomic::Quaternion( a.q.toRotationMatrix().inverse() * q.toRotationMatrix() ).vec();
      vec.block<3,1>(6,0) = dot_p - a.dot_p;
      vec(9) = omega(2) - a.omega(2);
      return vec;
    }    

    inline void reset() {
      p.setZero();
      q.setIdentity();
      dot_p.setZero();
      omega.setZero();
      dot_omega.setZero();
      ddot_p.setZero();
      dddot_p.setZero();
      ddddot_p.setZero();
      dot_psi = 0.f;
      thrust = 0.f;
      d_thrust= 0.f;
    }
    
    inline gnomic::BaseFlatState* getFlatState() const {
      return new Xi( p(0), p(1), p(2), gnomic::yawFromQuaternion(q) );      
    }

    inline gnomic::BaseFlatState* getDeltaFlatState() const  {
      return new Xi( dot_p(0), dot_p(1), dot_p(2), omega(2));    
    }

    
    void print() const{
      std::cerr << "p:         " << p.transpose() << std::endl
                << "qx: " << q.x() << " qy: " << q.y() << " qz: " << q.z() << " qw: " << q.w() << std::endl
                << "dp:        " << dot_p.transpose() << std::endl
                << "omega:     " << omega.transpose() << std::endl
                << "ddot_p:    " << ddot_p.transpose() << std::endl
                << "dddot_p:    " << dddot_p.transpose() << std::endl
                << "dot_psi:   " << dot_psi << std::endl
                << "thrust:    " << thrust << std::endl;
    }
    
  };
  
}
