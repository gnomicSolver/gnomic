#pragma once
#include <gnomic/utils.hpp>
#include <iostream>
#include <gnomic/base_flat_state.h>

namespace mav_model {
  
  struct Xi : public gnomic::BaseFlatState {
    gnomic::real x;
    gnomic::real y;
    gnomic::real z;
    gnomic::real psi;
    
    Xi() {
      x = 0;
      y = 0;
      z = 0;
      psi = 0;
    }

    const static int size = 4;

    gnomic::real* ptr(){
      return &x;
    }   
  
  Xi(const gnomic::real &x_, const gnomic::real &y_, const gnomic::real &z_, const gnomic::real &psi_)
    : x(x_), y(y_), z(z_) {
      psi = gnomic::normalizeAngle(psi_);
    }
  
  Xi(const Xi& Xi_) : x(Xi_.x), y(Xi_.y), z(Xi_.z), psi(Xi_.psi) {}

    inline void update(const gnomic::Vector4& Xi_ptr_) {
      
      x += Xi_ptr_(0);
      y += Xi_ptr_(1);
      z += Xi_ptr_(2);
      psi = gnomic::normalizeAngle( psi + Xi_ptr_(3));
    }

    // assignment operator modifies object, therefore non-const
    Xi &operator=(const Xi &a) {
      x = a.x;
      y = a.y;
      z = a.z;
      psi = gnomic::normalizeAngle(a.psi);
      return *this;
    }

    // addop. doesn't modify object. therefore const.
    Xi operator+(const Xi &a) const {
      return Xi(a.x + x, a.y + y, a.z + z,
                a.psi + psi); // check normalization maybe
    }

    Xi operator*(const gnomic::real &a) const {
      return Xi(a * x, a * y, a * z, a * psi); // check normalization maybe
    }

    // doesn't modify object. therefore const.
    Xi operator-(const Xi &a) const {
      return Xi(x - a.x, y - a.y, z - a.z,
                psi - a.psi); // check normalization maybe
    }


    // unNormalizes the FlatState to allow cubicHermiteInterpolation
    // i.e. the angle will have the same sign of FlatState a
    void unNormalize(const Xi&a) {
      if(gnomic::sgn(a.psi) != gnomic::sgn(psi) &&
         fabs(a.psi)+fabs(psi) > M_PI) {
        psi += gnomic::sgn(a.psi)*2*M_PI;
      }
    }
    
    void print() const{
      std::cout << "x: " << x << "  y: " << y << " z: " << z << " psi: " << psi
                << std::endl;
    }
  };
}
