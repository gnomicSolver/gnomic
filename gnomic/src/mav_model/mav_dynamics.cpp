
namespace mav_model{

  template<typename Input, typename State, typename FlatState>
  MavDynamics<Input, State, FlatState>::MavDynamics(const gnomic::real& mass,
                                                    const gnomic::Matrix3& inertia) : gnomic::BaseRobotDynamics<Input, State, FlatState>(mass, inertia) {
	
    /* PLACE DRAGMATRIX STUFF WITH YAML FILE */					      
    this->_dragMatrix.setZero();
    this->_dragMatrix(0,0) = 0.01;
    this->_dragMatrix(0,0) = 0.01;
  } 

  template<typename Input, typename State, typename FlatState>
  void MavDynamics<Input, State, FlatState>::computeState(State& state,
                                                          const State& state0,
                                                          const FlatState& xi0,
                                                          const FlatState& xif,
                                                          const gnomic::real& dt) {

    float dt_ = dt;
//     float beta = 0.f;
//     if(dt_ < 0.15)
//           beta = 0.9f;
//     
//     state.p = gnomic::Vector3(xif.x, xif.y, xif.z);    
//     state.dot_p =  state0.dot_p * beta + ( 1.f - beta) * (state.p - gnomic::Vector3(xi0.x, xi0.y, xi0.z))/dt_;
//     
//     gnomic::real& dot_psi = state.dot_psi;
//     state.dot_psi = state0.dot_psi * beta + ( 1.f - beta) * (gnomic::normalizeAngle(xif.psi - xi0.psi))/dt_;
    if(dt_ < .15)
      dt_ = .15;
    
    state.p = gnomic::Vector3(xif.x, xif.y, xif.z);
    state.dot_p =  (state.p - gnomic::Vector3(xi0.x, xi0.y, xi0.z))/dt_;
    
    gnomic::real& dot_psi = state.dot_psi;
    
    state.ddot_p =  (state.dot_p - state0.dot_p)/dt_;
    state.dot_psi = (gnomic::normalizeAngle(xif.psi - xi0.psi))/dt_;
    
    gnomic::Vector3 alpha(state.ddot_p - gnomic::gravity_vector);
    const gnomic::real c_psi = cos(xif.psi);
    const gnomic::real s_psi = sin(xif.psi);
    
    gnomic::Matrix3 R;
    R.setZero();
    R.col(0) = gnomic::Vector3(-s_psi, c_psi, 0).cross(alpha);
    R.col(0) /= R.col(0).norm();
    R.col(1) = alpha.cross(R.col(0));
    R.col(1) /= R.col(1).norm();
    R.col(2) = R.col(0).cross(R.col(1));
    R.col(2) /= R.col(2).norm();
    state.q = gnomic::Quaternion(R);
    state.q.normalize();
    
    state.thrust = this->_m*R.col(2).dot(alpha);
    state.d_thrust = (state.thrust - state0.thrust)/dt_;
    
    
    state.dddot_p = (state.ddot_p - state0.ddot_p)/dt_;
    state.ddddot_p = (state.dddot_p - state0.dddot_p)/dt_;
    
    gnomic::real ddot_psi = dot_psi - state0.dot_psi;
    ddot_psi /= dt;
    
    gnomic::Vector3& omega = state.omega;
    omega.setZero();
    omega(0) = -R.col(1).dot(state.dddot_p)/state.thrust;
    omega(1) = R.col(0).dot(state.dddot_p)/state.thrust;
    omega(2) = dot_psi * R(2,2);
    
    gnomic::Vector3& d_omega = state.dot_omega;
    d_omega.setZero();
    d_omega(0) = (-this->_m * R.col(1).dot(state.ddddot_p) - omega(1)*omega(2)*state.thrust + 2*omega(0)*state.d_thrust)/state.thrust;
    d_omega(1) = (this->_m * R.col(1).dot(state.ddddot_p) - omega(0)*omega(2)*state.thrust - 2*omega(1)*state.d_thrust)/state.thrust;
    d_omega(2) = ddot_psi * R(2,2) + dot_psi*gnomic::Vector3::UnitZ().transpose() * R * gnomic::skew(omega) * gnomic::Vector3::UnitZ();
  
    
  } 
  
  template<typename Input, typename State, typename FlatState>  
  void MavDynamics<Input, State, FlatState>::predictState(const State& statei0, 
                                                          const Input& input, 
                                                          State& stateif, 
                                                          const gnomic::real& dt) {
    
    stateif.omega = statei0.dot_omega*dt + statei0.omega;
    const gnomic::Vector3 omega_B( statei0.q.toRotationMatrix().inverse()*statei0.omega );   
    const gnomic::real c_qa = cos( omega_B(0)*dt );
    const gnomic::real c_qb = cos( omega_B(1)*dt );
    const gnomic::real c_qc = cos( omega_B(2)*dt );
    const gnomic::real s_qa = sin( omega_B(0)*dt );
    const gnomic::real s_qb = sin( omega_B(1)*dt );
    const gnomic::real s_qc = sin( omega_B(2)*dt );
    
    const gnomic::real w = c_qa * c_qb * c_qc + s_qa * s_qb * s_qc;
    const gnomic::real x = s_qa * c_qb * c_qc - c_qa * s_qb * s_qc;
    const gnomic::real y = c_qa * s_qb * c_qc + s_qa * c_qb * s_qc;
    const gnomic::real z = c_qa * c_qb * s_qc - s_qa * s_qb * c_qc;
    const gnomic::Quaternion q_update(w, x, y, z);
    stateif.q = statei0.q*q_update; 
    
    stateif.ddot_p = statei0.dddot_p*dt + statei0.ddot_p;
    stateif.dot_p = statei0.ddot_p*dt + statei0.dot_p;
    stateif.p = statei0.dot_p*dt + statei0.p;
    
//     gnomic::Vector3 k1, k2, k3, k4;
//     k1 = statei0.ddot_p;
//     k2 = statei0.ddot_p + dt/2*k1;
//     k3 = statei0.ddot_p + dt/2*k2;
//     k4 = statei0.ddot_p + dt*k3;
//     
//     stateif.dot_p = dt/6*(k1+k2+k3+k4) + statei0.dot_p;  
//     
//     k1 = statei0.dot_p;
//     k2 = statei0.dot_p + dt/2*k1;
//     k3 = statei0.dot_p + dt/2*k2;
//     k4 = statei0.dot_p + dt*k3;
//     
//     stateif.p = dt/6*(k1 + k2 + k3 + k4) + statei0.p; 
    
  }

}
