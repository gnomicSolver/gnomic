#pragma once  
#include <gnomic/base_input_vector.h>

namespace mav_model {

  struct InputVector : public gnomic::BaseInputVector{
    gnomic::real tau1;
    gnomic::real tau2;
    gnomic::real tau3;
    gnomic::real thrust;
    
    InputVector(){
      tau1 = 0;
      tau2 = 0;
      tau3 = 0;
      thrust = 0;
    }
    
    // This static variable stores the inputVector size
    // it has always to be defined!
    const static int size = 4;

    
    inline gnomic::Vector4 getVector() {
      return gnomic::Vector4( tau1, tau2, tau3, thrust );
    }
	    
    void print(){
      std::cerr << "tau1: " << tau1
                << " tau2: " << tau2
                << " tau3: " << tau3
                << " thrust: " << thrust << std::endl;
    }

  };
}
