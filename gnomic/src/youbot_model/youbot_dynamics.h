#pragma once
#include <gnomic/base_robot_dynamics.h>

namespace gnomic{
  
  template<typename Input, typename State, typename FlatState>
    class YoubotDynamics : public BaseRobotDynamics<Input, State, FlatState>{
    
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    YoubotDynamics(const gnomic::real& mass, const gnomic::Matrix3& inertia);
    
    ~YoubotDynamics(){
      std::cerr << "[YoubotDynamics]: deleted\n";
    };
       
    virtual void computeInput( const State& state,
                               Input& input) { 
      input.vx = state.d_p(0);
      input.vy = state.d_p(1);
      input.omega = state.yaw_rate;
    } 
    
    virtual void computeState( State& state,
                               const State& state0,
                               const FlatState& xi0,
                               const FlatState& xif, const gnomic::real& dt);
    
    virtual void predictState( const State& statei0, 
                               const Input& input, 
                               State& stateif, const gnomic::real& dt );
    
    virtual void computeConstraints(const FlatState& x0,
                                    const FlatState& xf,
                                    gnomic::MatrixX& constraint,
                                    const gnomic::real& dt){};
    
  private:

    
  };
  

}

#include <youbot_model/youbot_dynamics.cpp>
