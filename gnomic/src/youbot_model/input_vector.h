#pragma once  
#include <gnomic/base_input_vector.h>

namespace youbot_model {
  
  struct InputVector : public gnomic::BaseInputVector{
    gnomic::real vx;
    gnomic::real vy;
    gnomic::real omega;
    
    InputVector(){
      vx = 0;
      vy = 0;
      omega = 0;
    }
    
    // This static variable stores the inputVector size
    // it has always to be defined!
    const static int size = 3;

    inline gnomic::Vector3 getVector() {
      return gnomic::Vector3( vx, vy, omega );
    }
    
    void print(){
      std::cout << "vx: " << vx << " vy: " << vy << " omega: " << omega << std::endl;
    }
    
  };
}
