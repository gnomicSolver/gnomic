#pragma once
#include <gnomic/utils.hpp>
#include <gnomic/base_state_vector.h>
#include "xi.h"

namespace youbot_model{
  
  struct StateVector : public gnomic::BaseStateVector {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    
    gnomic::Vector2 p;
    gnomic::real yaw;
    gnomic::Vector2 d_p;
    gnomic::real yaw_rate;
    gnomic::Vector2 dd_p;
   
    const static int size = 5;
    const static int constraints_size = 0;
    typedef Eigen::Matrix<gnomic::real, size, 1> StateErrorVector;

    ~StateVector() {
      //std::cerr << "[StateVector]: deleted\n";
    }
    
    StateVector() {
      p.setZero();
      yaw = 0;
      d_p.setZero();
      yaw_rate = 0;
      dd_p.setZero();
      //       d_p_B.setZero();
    }
    
    StateVector( const StateVector& state) {
      p = state.p;
      yaw = state.yaw;
      d_p = state.d_p;
      yaw_rate = state.yaw_rate;
      dd_p = state.dd_p;
    }
    
    StateErrorVector operator-(const StateVector& a) const {
      StateErrorVector result;
      result.block<2,1>(0,0) = p - a.p;
      result(2) = yaw - a.yaw;
      result.block<2,1>(3,0) = d_p - a.d_p;
      return result;
      
      /*StateErrorVector vec;
        vec.setZero();
        vec.block<2,1>(0,0) = p - a.p;
        vec(2,0) = yaw - a.yaw;*/
      //       return vec;
    }    

    inline void reset() {
      p.setZero();
      d_p.setZero();
      dd_p.setZero();
      yaw_rate = 0.f;
      yaw = 0.f;
      /*gnomic::real yaw; //orientation
        dot_p.setZero();
        gnomic::real yaw_rate; //angular velocity
        ddot_p.setZero();
        dot_p_B.setZero();*/
    }
    
    inline gnomic::BaseFlatState* getFlatState() const {
      return new youbot_model::Xi( p(0), p(1), yaw );
    }

    inline gnomic::BaseFlatState* getDeltaFlatState() const {
      return new youbot_model::Xi( d_p(0), d_p(1), yaw_rate );
    }

    
    void print() const {
      std::cout << p.transpose() << " " << yaw << " " << d_p.transpose() << " " << yaw_rate << " " << dd_p.transpose() << std::endl;
    }
    
  };
  
}
