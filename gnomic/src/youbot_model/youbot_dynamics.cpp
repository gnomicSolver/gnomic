
namespace gnomic {
  
  template<typename Input, typename State, typename FlatState>
  YoubotDynamics<Input, State, FlatState>::YoubotDynamics(const gnomic::real& mass,
                                                          const gnomic::Matrix3& inertia) : BaseRobotDynamics<Input, State, FlatState>(mass, inertia) {
  } 
  
  template<typename Input, typename State, typename FlatState>
  void YoubotDynamics<Input, State, FlatState>::computeState(State& state,
                                                             const State& state0,
                                                             const FlatState& xi0,
                                                             const FlatState& xif,
                                                             const gnomic::real& dt) {
        
    state.p = Vector2(xif.x, xif.y);
    state.yaw = xif.yaw;
    
    gnomic::Matrix2 world_to_body;
    world_to_body =  gnomic::Rotation2D(state.yaw);
        
    state.d_p = (world_to_body.inverse()*state.p - world_to_body.inverse()*state0.p)/dt;
    
    state.dd_p = (state.d_p - state0.d_p)/dt;
    state.yaw_rate = (state.yaw - state0.yaw)/dt;
  } 
  
  template<typename Input, typename State, typename FlatState>
  void YoubotDynamics<Input, State, FlatState>::predictState(const State& statei0, 
                                                             const Input& input, 
                                                             State& stateif, 
                                                             const gnomic::real& dt) {
    
    stateif.d_p = statei0.d_p + .5*(statei0.dd_p + stateif.dd_p)*dt;
    stateif.yaw = statei0.yaw + statei0.yaw_rate*dt;
    
    gnomic::Matrix2 world_to_body;
    world_to_body =  gnomic::Rotation2D(statei0.yaw);
    
    stateif.p = statei0.p + .5*world_to_body*(stateif.d_p+statei0.d_p)*dt;
  }

}
