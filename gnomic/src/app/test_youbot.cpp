#include <iostream>
#include <gnomic/solver.h>
#include <gnomic/loss.h>

#include <youbot_model/youbot_dynamics.h>
#include <youbot_model/input_vector.h>
#include <youbot_model/state_vector.h>
#include <youbot_model/xi.h>

using namespace gnomic;
using namespace youbot_model;

typedef gnomic::Loss<youbot_model::InputVector,
                     youbot_model::StateVector,
                     youbot_model::Xi> YoubotLoss;
typedef gnomic::YoubotDynamics<youbot_model::InputVector,
                               youbot_model::StateVector,
                               youbot_model::Xi> Youbot;



int main(){

  gnomic::real mass = 2.f;
  
  //declare a Youbot
  Youbot* youbot = new Youbot(mass, gnomic::Matrix3::Identity()); 
  //declare a Youbot Based Loss
  YoubotLoss loss;
  loss.setRobotDynamics(youbot);
  //declare a Solver
  Solver<YoubotLoss> solver;

  InputVector init_input;
  StateVector init_state;
  StateVector final_state;

  gnomic::real tf = 5;
  int N = 10;
  
  final_state.p = gnomic::Vector2(1.f, -1.f);
  final_state.yaw = 1.f;
  
  solver.setLoss(&loss);
  solver.setInitialInput(&init_input);
  solver.setInitialState(&init_state);
  solver.setFinalState(&final_state);
  solver.setTimeVariables(tf, N);
  solver.init(TrajectoryInitialization::LINEAR);
  solver.setVerbosity(true);
  // max_pyramidal_refs = 2, epsilon = 1e-4
  solver.setTimeMeshRefinement(true, 2, 1e-4f);
  solver.compute();

  std::vector<Xi> solution = solver.optimalTrajectory();

  const Profiler& profiler = solver.profiler();
  std::cerr << profiler << std::endl;
  
  delete youbot;
  return 0;
}
