#include <iostream>
#include <gnomic/solver.h>
#include <gnomic/loss_nonholonomic.h>

#include <turtlebot_model/turtlebot_dynamics.h>
#include <turtlebot_model/input_vector.h>
#include <turtlebot_model/state_vector.h>
#include <turtlebot_model/xi.h>

using namespace gnomic;
using namespace turtlebot_model;

typedef gnomic::Loss<turtlebot_model::InputVector,
                     turtlebot_model::StateVector,
                     turtlebot_model::Xi> TurtlebotLoss;
typedef gnomic::TurtlebotDynamics<turtlebot_model::InputVector,
                                  turtlebot_model::StateVector,
                                  turtlebot_model::Xi> Turtlebot;



int main(){

  gnomic::real mass = 5.f;
  
  //declare a Turtlebot
  Turtlebot* turtlebot = new Turtlebot(mass, gnomic::Matrix3::Identity()); 
  //declare a Turtlebot Based Loss
  TurtlebotLoss loss;
  loss.setRobotDynamics(turtlebot);
  //declare a Solver
  Solver<TurtlebotLoss> solver;

  InputVector init_input;
  StateVector init_state;
  StateVector final_state;

  gnomic::real tf = 5;
  int N = 10;
  
  final_state.p = gnomic::Vector2(1.f, -1.f);
  final_state.yaw = 1.f;
  
  solver.setLoss(&loss);
  solver.setInitialInput(&init_input);
  solver.setInitialState(&init_state);
  solver.setFinalState(&final_state);
  solver.setTimeVariables(tf, N);
  solver.init(TrajectoryInitialization::LINEAR);
  solver.setVerbosity(true);
  // max_pyramidal_refs = 2, epsilon = 1e-4
  solver.setTimeMeshRefinement(true, 2, 1e-4f);
  solver.compute();

  std::vector<Xi> solution = solver.optimalTrajectory();

  const Profiler& profiler = solver.profiler();
  std::cerr << profiler << std::endl;

  delete turtlebot;
  return 0;
}
