#include <iostream>
#include <gnomic/solver.h>
#include <gnomic/loss.h>
#include <gnomic/profiler.h>

#include <mav_model/mav_dynamics.h>
#include <mav_model/input_vector.h>
#include <mav_model/state_vector.h>
#include <mav_model/xi.h>

using namespace gnomic;
using namespace mav_model;

typedef gnomic::Loss<mav_model::InputVector,
                     mav_model::StateVector,
                     mav_model::Xi> MavLoss;
typedef mav_model::MavDynamics<mav_model::InputVector,
                               mav_model::StateVector,
                               mav_model::Xi> Mav;



int main(){

  gnomic::real mass = 1.545f;
  gnomic::Matrix3 inertia_matrix;
  inertia_matrix.setIdentity();
  inertia_matrix(0,0) = 0.01f;
  inertia_matrix(1,1) = 0.01f;
  inertia_matrix(2,2) = 0.06f;

  
  //declare a Mav
  Mav* mav = new Mav(mass, inertia_matrix); 
  //declare a Mav Based Loss
  MavLoss loss;
  loss.setRobotDynamics(mav);

  MavLoss::Weights weights;
  weights.Sf.diagonal() << 300, 300, 300,
    1, 1, 150,
    50, 50, 50, 1;
  weights.Al.diagonal() << 5, 5, 5,
    5, 5, 5, 7, 7, 7, 5;
  weights.R.diagonal() << 1, 1, 1, 1;
  loss.setWeights(weights);

  //declare a Solver
  Solver<MavLoss> solver;

  InputVector init_input;
  StateVector init_state;
  StateVector final_state;

  gnomic::real tf = 2.f;
  int N = 40;
  
  final_state.p = gnomic::Vector3(1.f, 2.f, 4.f);
  // final_state.q
  
  solver.setLoss(&loss);
  solver.setInitialInput(&init_input);
  solver.setInitialState(&init_state);
  solver.setFinalState(&final_state);
  solver.setTimeVariables(tf, N);
  solver.init(TrajectoryInitialization::LINEAR);
  solver.setVerbosity(true);
  solver.setIterations(10, false);
  // max_pyramidal_refs = 2, epsilon = 1e-4
  solver.setTimeMeshRefinement(true, 1, 1e-4f);
  solver.compute();

  std::vector<Xi> solution = solver.optimalTrajectory();
  std::cerr << KYEL << "Final Solution: \n";
  for(size_t i = 0; i < solution.size(); ++i)
    solution[i].print();
  std::cerr << RST;
  
  StateVector state_solution = solver.optimalTrajectory(0.1);
  //  std::cerr << FGRN("[GNOMIC][TestMAV]: state at 0.1: ") << std::endl;
  //  state_solution.print();
  
  const Profiler& profiler = solver.profiler();
  //  std::cerr << profiler << std::endl;

  delete mav;
  return 0;
}
