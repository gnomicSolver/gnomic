#include <iostream>
#include <gnomic/solver.h>
#include <gnomic/loss.h>
#include <gnomic/profiler.h>
#include <gnomic/colors.h>

#include <mav_model/mav_dynamics.h>
#include <mav_model/input_vector.h>
#include <mav_model/state_vector.h>
#include <mav_model/xi.h>

#include <fstream>

using namespace gnomic;
using namespace mav_model;

void printBanner(const char** banner_) {
  const char** b = banner_;
  while(*b) {
    std::cerr << *b << std::endl;
    b++;
  }
}

const char* banner[] = {
  "\n\nUsage:   "
  "\n",
  "Example:   ", "Options:\n", "------------------------------------------\n",
  "-timeMesh <flag>            use timeMeshRefinement",
  "-iterations <int>           number of max iteration",
  "-all <flag>                 perform all the iterations without considering criteria for stop",
  "-trajectories <int>         number of trajectories per test, default 4000",
  "-snap <float>               dt between snaps, default 0.2",
  "-o <string>                 filename where to write the output",
  "-h                          this help\n",
  0
};

typedef gnomic::Loss<mav_model::InputVector,
                     mav_model::StateVector,
                     mav_model::Xi> MavLoss;
typedef mav_model::MavDynamics<mav_model::InputVector,
                               mav_model::StateVector,
                               mav_model::Xi> Mav;





int main(int argc, char** argv) {

  bool use_timeMesh = false;
  int iterations = 10;
  bool all = false;
  std::string filename = "gnomic_profiling_output.txt";
  gnomic::real snap = 0.2f;
  int trajectories = 4000;

  
  int c = 1;
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-timeMesh")) {
      use_timeMesh = true;
    } else if (!strcmp(argv[c], "-all")) {
      all = true;
    } else if (!strcmp(argv[c], "-iterations")) {
      c++;
      iterations = std::stoi(argv[c]);
    } else if (!strcmp(argv[c], "-trajectories")) {
      c++;
      trajectories = std::stoi(argv[c]);
    } else if (!strcmp(argv[c], "-snap")) {
      c++;
      snap = std::stof(argv[c]);
    } else if (!strcmp(argv[c], "-o")) {
      c++;
      filename = argv[c];
    }
    c++;
  }

  std::cerr << FGRN("Chosen Options:\n");
  std::cerr << FGRN("-timeMesh     "); use_timeMesh ? std::cerr << "true\n" : std::cerr << "false\n";
  std::cerr << FGRN("-iterations   ") << iterations << std::endl;
  std::cerr << FGRN("-all          "); all ? std::cerr << "true\n" : std::cerr << "false\n";
  std::cerr << FGRN("-snap         ") << snap << std::endl;
  std::cerr << FGRN("-trajectories ") << trajectories << std::endl;
  std::cerr << FGRN("-filename     ") << filename << std::endl;
  
  std::ofstream writer(filename);
  
  gnomic::real mass = 1.545f;
  gnomic::Matrix3 inertia_matrix;
  inertia_matrix.setIdentity();
  inertia_matrix(0,0) = 0.01f;
  inertia_matrix(1,1) = 0.01f;
  inertia_matrix(2,2) = 0.06f;

  
  //declare a Mav
  Mav* mav = new Mav(mass, inertia_matrix); 
  //declare a Mav Based Loss
  MavLoss loss;
  loss.setRobotDynamics(mav);

  MavLoss::Weights weights;
  weights.Sf.diagonal() << 300, 300, 300,
    1, 1, 150,
    50, 50, 50, 1;
  weights.Al.diagonal() << 5, 5, 5,
    5, 5, 5, 7, 7, 7, 5;
  weights.R.diagonal() << 1, 1, 1, 1;
  loss.setWeights(weights);
  //declare a Solver
  Solver<MavLoss> solver;

  InputVector init_input;
  StateVector init_state;
  StateVector final_state;

  
  final_state.p = gnomic::Vector3(1.f, -1.f, 2.f);
  // final_state.q
  
  solver.setLoss(&loss);
  solver.setInitialInput(&init_input);
  solver.setInitialState(&init_state);
  solver.setFinalState(&final_state);

  solver.setVerbosity(false);
  
  gnomic::real min_time = .5f;
  gnomic::real max_time = 4.f;
  gnomic::real step_time = .2f;
  gnomic::real snapshot_time = snap;
  int number_of_trajectory_per_timeset = trajectories;

  solver.setIterations(iterations, all); // fixed number of iteration!
  if(use_timeMesh){
    solver.setTimeMeshRefinement(true, 1, 1e-4f);
  }
  
  std::vector<std::string> profiler_elements;
  for(gnomic::real tf = min_time; tf <= max_time + 1e-4; tf += step_time) {
    profiler_elements.push_back(std::to_string(tf));
  }
  gnomic::Profiler profiler(profiler_elements); 
  
  int N = min_time/snapshot_time;
  for(gnomic::real tf = min_time; tf <= max_time + 1e-4; tf += step_time) {


    //    if(N < 3) continue;
    std::cerr << FGRN("profiling tf: ") << tf
              << FGRN(" N: ") << N
              << FGRN(" trajectories: ") << number_of_trajectory_per_timeset
              << std::endl;
    for(int i = 0; i < number_of_trajectory_per_timeset; ++i) {
      // re-init the trajectory
      solver.setTimeVariables(tf, N);
      solver.init(TrajectoryInitialization::LINEAR);

      profiler.element(std::to_string(tf)).start();
      solver.compute();
      profiler.element(std::to_string(tf)).stop();
    }
    std::cerr << BOLD(FRED(".\n"));
    const gnomic::Element& current_element = profiler.element(std::to_string(tf));
    std::cerr << current_element << std::endl;
    const float mean = current_element.tot / (double)current_element.calls;
    writer << std::to_string(tf) << " " << N << " " << mean << std::endl;
    ++N;
  }

  std::cerr << profiler << std::endl;
  writer.close();
  
  delete mav;
  return 0;
}
