#include <gnomic/utils.hpp>
#include <gnomic/profiler.h>

#include <turtlebot_model/turtlebot_dynamics.h>
#include <turtlebot_model/input_vector.h>
#include <turtlebot_model/state_vector.h>
#include <turtlebot_model/xi.h>

using namespace gnomic;
using namespace turtlebot_model;


int main(){

  std::vector<std::string> names;
  names.push_back("predictState");
  names.push_back("cubicHermite");
  names.push_back("print");
  Profiler profiler(names);
  
  StateVector state0;
  state0.p = gnomic::Vector2::Zero();
  state0.dot_p = gnomic::Vector2::Zero();
  state0.ddot_p = gnomic::Vector2::Zero();
  state0.yaw = 0.f;
  state0.yaw_rate = 0.f;
  state0.yaw_acc = 0.f;


  InputVector u;
  u.v = .1;
  u.omega = 0.f;

  TurtlebotDynamics<InputVector, StateVector,Xi> robot(1.f, gnomic::Matrix3::Identity());
  
  StateVector state1;
  
  profiler.element("predictState").start();
  robot.predictState(state0, u, state1, 0.1);
  profiler.element("predictState").stop();
  
  std::cerr << "s0: " << std::endl;
  state0.print();
  std::cerr << "s1: " << std::endl;
  state1.print();
  
  for(int i=1; i < 10; ++i) {
    Xi result;
    profiler.element("cubicHermite").start();
    puntualCubicHermiteInterpolation(result, state0, state1, 0.1, gnomic::real(i)/10);
    profiler.element("cubicHermite").stop();
    std::cerr << "i: ";
    profiler.element("print").start();
    result.print();
    profiler.element("print").stop();
  }

  std::cerr << "PROFILER RESULT\n" << profiler << std::endl;
  
  return 0;
}
