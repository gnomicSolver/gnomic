#pragma once
#include <gnomic/utils.hpp>
#include <gnomic/base_state_vector.h>
#include "xi.h"

namespace turtlebot_model{
  
  struct StateVector : public gnomic::BaseStateVector {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    
    gnomic::Vector2 p;
    gnomic::Vector2 dot_p;
    gnomic::Vector2 ddot_p;
    
    gnomic::real yaw;
    gnomic::real yaw_rate;
    gnomic::real yaw_acc;

    gnomic::real acc;
    gnomic::real v;
   
    const static int size = 4;
    const static int constraints_size = 1;
    typedef Eigen::Matrix<gnomic::real, size, 1> StateErrorVector;

    ~StateVector() {
      //std::cerr << "[StateVector]: deleted\n";
    }
    
    StateVector() {
      p.setZero();
      yaw = 0.f;
      yaw_rate = 0.f;
      yaw_acc = 0.f;
      acc = 0.f;
      v = 0.f;
      dot_p.setZero();
      ddot_p.setZero();      
    }
    
    StateVector( const StateVector& state) {
      p = state.p;
      yaw = state.yaw;
      v = state.v;
      yaw_rate = state.yaw_rate;
      yaw_acc  = state.yaw_acc;
      acc = state.acc;
      dot_p = state.dot_p;
      ddot_p = state.ddot_p;
    }
    
    StateErrorVector operator-(const StateVector& a) const {
      StateErrorVector result;
      result.block<2,1>(0,0) = p - a.p;
      result(2) = yaw - a.yaw;
      result(3) = v - a.v;
      return result;
    }    

    inline void reset() {
      p.setZero();
      v = 0.f;
      acc = 0.f;
      yaw_rate = 0.f;
      yaw_acc = 0.f;

      yaw = 0.f;
      dot_p.setZero();
      ddot_p.setZero();
    }
    
    inline gnomic::BaseFlatState* getFlatState() const {      
      return new turtlebot_model::Xi( p(0), p(1), yaw );      
    }

    inline gnomic::BaseFlatState* getDeltaFlatState() const {      
      return new turtlebot_model::Xi( dot_p(0), dot_p(1), yaw_rate );      
    }
    
    void print() const{
      std::cout << p.transpose() << " " << yaw << " " << v << " " << yaw_rate << " " << acc << std::endl;
    }
    
  };
  
}
