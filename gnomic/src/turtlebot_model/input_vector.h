#pragma once  
#include <gnomic/base_input_vector.h>

namespace turtlebot_model {
  
  struct InputVector : public gnomic::BaseInputVector{
    gnomic::real v;
    gnomic::real omega;
    
    InputVector(){
      v = 0;
      omega = 0;
    }
    
    // This static variable stores the inputVector size
    // it has always to be defined!
    const static int size = 2;

    inline gnomic::Vector2 getVector() {
      return gnomic::Vector2( v, omega );
    }
    
    void print(){
      std::cout << "v: " << v << " omega: " << omega << std::endl;
    }
    
  };
}
