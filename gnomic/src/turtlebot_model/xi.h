#pragma once
#include <gnomic/utils.hpp>
#include <iostream>
#include <gnomic/base_flat_state.h>

namespace turtlebot_model {
  
  struct Xi : public gnomic::BaseFlatState{
    gnomic::real x;
    gnomic::real y;
    gnomic::real yaw;
    
    Xi() {
      x = 0;
      y = 0;
      yaw = 0;
    }

    const static int size = 3;
    gnomic::real* ptr(){
      return &x;
    }   
  
  Xi(const gnomic::real &x_, const gnomic::real &y_, const gnomic::real &yaw_)
    : x(x_), y(y_), yaw(yaw_){}
  
  Xi(const Xi& Xi_) : x(Xi_.x), y(Xi_.y), yaw(Xi_.yaw) {}

    inline void update(const gnomic::Vector3& Xi_ptr_) {
      x += Xi_ptr_(0);
      y += Xi_ptr_(1);     
      yaw += Xi_ptr_(2);
    }

    // assignment operator modifies object, therefore non-const
    Xi &operator=(const Xi &a) {
      x = a.x;
      y = a.y;
      yaw = a.yaw;
      return *this;
    }

    // addop. doesn't modify object. therefore const.
    Xi operator+(const Xi &a) const {
      return Xi(a.x + x, a.y + y, a.yaw + yaw); // check normalization maybe
    }

    Xi operator*(const gnomic::real &a) const {
      return Xi(a * x, a * y, a * yaw); // check normalization maybe
    }

    // doesn't modify object. therefore const.
    Xi operator-(const Xi &a) const {
      return Xi(x - a.x, y - a.y, yaw - a.yaw); // check normalization maybe
    }

    // unNormalizes the FlatState to allow cubicHermiteInterpolation
    // i.e. the angle will have the same sign of FlatState a
    void unNormalize(const Xi&a) {
      
    }
    
    void print() const{
      std::cout << "x: " << x << "  y: " << y << " yaw: " << yaw <<  std::endl;
    }
  };
}
