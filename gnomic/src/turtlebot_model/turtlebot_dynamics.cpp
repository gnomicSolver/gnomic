
namespace gnomic {
  
  template<typename Input, typename State, typename FlatState>
  TurtlebotDynamics<Input, State, FlatState>::TurtlebotDynamics(const gnomic::real& mass,
                                                                const gnomic::Matrix3& inertia) : BaseRobotDynamics<Input, State, FlatState>(mass, inertia) {
  } 
  
  template<typename Input, typename State, typename FlatState>
  void TurtlebotDynamics<Input, State, FlatState>::computeState(State& state,
                                                                const State& state0,
                                                                const FlatState& xi0,
                                                                const FlatState& xif,
                                                                const gnomic::real& dt) {
        
    state.p = Vector2(xif.x, xif.y);
    state.yaw = xif.yaw;
    
    gnomic::real dx,dy;
    
    dx = (xif.x - xi0.x)/dt;
    dy = (xif.y - xi0.y)/dt;

    state.dot_p.x() = dx;
    state.dot_p.y() = dy;
    
    state.v = cos(state.yaw)*dx + sin(state.yaw)*dy; //sqrt( dx*dx + dy*dy );
     
    state.acc = (state.v - state0.v)/dt;
    state.yaw_rate = (state.yaw - state0.yaw)/dt;
    state.yaw_acc = (state.yaw_rate - state0.yaw_rate)/dt;

    state.ddot_p = (state.dot_p - state0.dot_p)/dt;
    
  } 
  
  template<typename Input, typename State, typename FlatState>
  void TurtlebotDynamics<Input, State, FlatState>::predictState(const State& statei0, 
                                                                const Input& input, 
                                                                State& stateif, 
                                                                const gnomic::real& dt) {
    
    
    stateif.v = statei0.v + statei0.acc*dt;
    stateif.yaw = statei0.yaw + input.omega*dt;
    
    stateif.p(0) = statei0.p(0) + input.v*cos(stateif.yaw)*dt;
    stateif.p(1) = statei0.p(1) + input.v*sin(stateif.yaw)*dt;

    stateif.dot_p = stateif.dot_p + statei0.ddot_p*dt;
    stateif.yaw_rate = stateif.yaw_rate + statei0.yaw_acc*dt;
    
  }
  
  template<typename Input, typename State, typename FlatState>
  void TurtlebotDynamics<Input, State, FlatState>::computeConstraints(const FlatState& x0,
                                                                      const FlatState& xf,
                                                                      gnomic::MatrixX& constraint,
                                                                      const gnomic::real& dt) {
  
    constraint.setIdentity(State::constraints_size, State::constraints_size);
    
    gnomic::real dx = (xf.x - x0.x)/dt;
    gnomic::real dy = (xf.y - x0.y)/dt;
    constraint(0,0) = -sin(xf.yaw)*dx + cos(xf.yaw)*dy; 
      
  };
  

}
